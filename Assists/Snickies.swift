//
//  Snickies.swift
//  Assists
//
//  Created by Thunpisit Amnuaikiatloet on 9/4/19.
//  Copyright © 2019 Thunpisit Amnuaikiatloet. All rights reserved.
//

import Foundation
import SwiftDate
import UIKit

/////////////////////////////////////
// Snickies/thompson RESTful APIs v1.0
/////////////////////////////////////

// Base URL for request
let baseUrl = "https://www.snickies.com/thompson"

// GET method retreiving user key with given authentication
// Params: email, pass
// https://www.snickies.com/thompson/user/signin
func requestSignin(email: String, pass: String, completionHandler: @escaping (_ result: String, _ error: String) -> Void) {
    
    let requestUrl = baseUrl + "/user/signin?email=" + email + "&pass=" + pass
    print("=> Request: " + requestUrl)
    
    guard let url = URL(string: requestUrl) else {return}
    let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
        guard let dataResponse = data,
            error == nil else {
                print(error?.localizedDescription ?? "Response Error")
                completionHandler("null", error?.localizedDescription ?? "Response Error")
                return }
        do{
            let jsonResponse = try JSONSerialization.jsonObject(with:
                dataResponse, options: [])
            //            print(jsonResponse) //Response result
            
            guard let jsonArray = jsonResponse as? [String: Any] else {
                return
            }
                        print(jsonArray)
            
            // Do shit here
            guard let res = jsonArray["return"] as? Bool else { return }
            
            if(res == true){
                guard let key = jsonArray["messsage"] as? String else { return }
                completionHandler(key, "Key obtained from server")
            } else {
                completionHandler("null", "Credentail is invalid")
            }
            
        } catch let parsingError {
            print("Error", parsingError)
            completionHandler("error", parsingError as! String)
        }
    }
    
    task.resume()
}

// GET method retrieveing specific patient information
// Params: key, pid
// https://www.snickies.com/thompson/patient/info
func requestPatient(key: String, pid: String, completionHandler: @escaping (_ result: Patient) -> Void) {
    
    let requestUrl = baseUrl + "/patient/info?key=" + key + "&pid=" + pid
    print("=> Request: " + requestUrl)
    
    guard let url = URL(string: requestUrl) else {return}
    let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
        guard let dataResponse = data,
            error == nil else {
                print(error?.localizedDescription ?? "Response Error")
                return }
        do{
            let jsonResponse = try JSONSerialization.jsonObject(with:
                dataResponse, options: [])
            //                        print(jsonResponse) //Response result
            
            guard let jsonArray = jsonResponse as? [String: Any] else {
                return
            }
            print(jsonArray)
            
            guard let res = jsonArray["return"] as? Bool else { return }
            //            print("=> /user/info return: " + String(res))
            
            if(res == true){
                
                if let info = jsonArray["data"] as? [String: Any] {
                    
                    if let id = info["_id"] as? String,
                        let active = info["active"] as? Bool,
                        let consent = info["consent"] as? Bool,
                        let firstname = info["firstname"] as? String,
                        let lastname = info["lastname"] as? String,
                        let nickname = info["nickname"] as? String,
                        let blood = info["blood"] as? String,
                        let institution = info["institution"] as? String,
                        let institutionId = info["identification"] as? String,
                        let doctor = info["doctor"] as? String,
                        let dob = info["dob"] as? String {
                        
                        completionHandler(Patient(id: id, active: active, consent: consent, firstname: firstname, lastname: lastname, nickname: nickname, dob: dob, gender: "unisex", blood: blood, institutionId: institutionId, institution: institution, doctor: doctor))
                    }
                }
                
            } else {
                // Return null UserStruct
            }
        } catch let parsingError {
            print("Error", parsingError)
        }
    }
    
    task.resume()
}

// GET method retrieveing user data with given key
// Params: key
// https://api.mythompson.org/user/info
func requestUser(key: String, completionHandler: @escaping (_ result: User) -> Void){
    
    let requestUrl = baseUrl + "/user/info?key=" + key
    print("=> Request: " + requestUrl)
    
    guard let url = URL(string: requestUrl) else {return}
    let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
        guard let dataResponse = data,
            error == nil else {
                print(error?.localizedDescription ?? "Response Error")
                return }
        do{
            let jsonResponse = try JSONSerialization.jsonObject(with:
                dataResponse, options: [])
            //                                    print(jsonResponse) //Response result
            
            guard let jsonArray = jsonResponse as? [String: Any] else {
                return
            }
            //            print(jsonArray)
            
            guard let res = jsonArray["return"] as? Bool else { return }
            print("=> /user/info return: " + String(res))
            
            if(res == true){
                
                if let data = jsonArray["data"] as? [String: Any] {
                    
                    if let firstname = data["firstname"] as? String,
                        let lastname = data["lastname"] as? String,
                        let email = data["email"] as? String,
                        let phone = data["phone"] as? String,
                        let role = data["role"] as? String,
                        let active = data["active"] as? Bool,
                        let consent = data["consent"] as? Bool,
                        let id = data["_id"] as? String,
                        let key = data["key"] as? String {
                        
                        if let patients = data["patients"] as? [String] {
                            completionHandler(User(id: id, role: role, active: active, consent: consent, email: email, key: key, firstname: firstname, lastname: lastname, phone: phone, patients: patients))
                        } else {
                            completionHandler(User(id: id, role: role, active: active, consent: consent, email: email, key: key, firstname: firstname, lastname: lastname, phone: phone, patients: []))
                        }
                        
                    }
                }
                
            } else {
                // In the future respond with empty Parent and error message
            }
            
        } catch let parsingError {
            print("Error", parsingError)
        }
    }
    
    task.resume()
}

// GET method retrieveing user data with given patient id and key
// Params: key, pid
// https://www.snickies.com/thompson/patient/events
func requestEvents(pid: String, key: String, completionHandler: @escaping (_ result: [Event]) -> Void){
    
    var events = [Event]()
    //    var event: Event
    //    var events: [Event]? = []
    
    let requestUrl = baseUrl + "/patient/events?key=" + key + "&pid=" + pid
    print("=> Request: " + requestUrl)
    
    guard let url = URL(string: requestUrl) else {return}
    let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
        guard let dataResponse = data,
            error == nil else {
                print(error?.localizedDescription ?? "Response Error")
                return }
        do{
            let jsonResponse = try JSONSerialization.jsonObject(with:
                dataResponse, options: [])
            //            print(jsonResponse)
            
            guard let jsonArray = jsonResponse as? [String: Any] else {
                return
            }
            //            print(jsonArray)
            
            guard let res = jsonArray["return"] as? Bool else { return }
            
            if(res == true){
                
                //                guard let data = jsonArray["data"] as? [[String: Any]] else { return }
                //                print(data)
                
                if let data = jsonArray["data"] as? [[String: Any]] {
                    //                    print("=> Data")
                    //                    print(data)
                    
                    for eachday in data {
                        
                        let day = eachday["created_date"] as? String
                        
                        var event = Event(date: day!)
                        
                        //                        print("=> Day")
                        //                        print(eachday)
                        
                        if let activities = eachday["activities"] as? [AnyObject] {
                            //                            print("=> Activities")
                            //                            print(activities)
                            
                            for activity in activities {
                                
                                let type = activity["type"] as? String
                                let subtype = activity["subtype"] as? String
                                let comment = activity["comment"] as? String
                                let created = activity["date_created"] as? String
                                let rating = activity["rating"] as? String
                                let ratingdes = activity["rating_description"] as? String
                                
                                event.activities.append(Activity(date: created!, type: type!, subtype: subtype!, rating: rating!, description: ratingdes!, notes: comment!))
                            }
                        }
                        
                        if let sleeps = eachday["sleeps"] as? [AnyObject] {
                            //                            print("=> Sleeps")
                            //                            print(sleeps)
                            
                            for sleep in sleeps {
                                
                                let duration = sleep["duration"] as? String
                                let depth = sleep["depth"] as? String
                                let interuptions = sleep["interuptions"] as? String
                                let snoring = sleep["snoring"] as? String
                                let time_to_sleep = sleep["time_to_sleep"] as? String
                                let time_to_up = sleep["time_to_up"] as? String
                                let comment = sleep["comment"] as? String
                                let created = sleep["date_created"] as? String
                                
                                event.sleeps.append(Sleep(date: created!, duration: duration!, depth: depth!, interuption: interuptions!, snoring: snoring!, timeToSleep: time_to_sleep!, timeToWake: time_to_up!, notes: comment!))
                            }
                        }
                        
                        if let medicines = eachday["medicines"] as? [AnyObject] {
                            //                            print("=> Medicines")
                            //                            print(medicines)
                            
                            for medicine in medicines {
                                
                                let name = medicine["name"] as? String
                                let strength = medicine["strength"] as? String
                                let quantity = medicine["quantity"] as? String
                                let time_of_day = medicine["time_of_day"] as? String
                                let comment = medicine["comment"] as? String
                                let created = medicine["date_created"] as? String
                                
                                event.medicines.append(Medicine(date: created!, name: name!, type: "unknown", dosage: strength!, quantity: quantity!, timeTaken: time_of_day!, notes: comment!))
                            }
                        }
                        events.append(event)
                    }
                    //                    print("=> Events")
                    //                    print(events)
                    completionHandler(events)
                }
            }
            
        } catch let parsingError {
            print("Error", parsingError)
        }
    }
    
    task.resume()
}

// POST method creating new user and key after valid invitation code
// https://api.mythompson.org/user/signup
func requestSignup(email: String, pass: String, fname: String, lname: String, phone: String, code: String, completionHandler: @escaping (_ result: Bool) -> Void){
    
    let raw_requestUrl = baseUrl + "/user/signup?email=" + email + "&pass=" + pass + "&code=" + code
    
    let encode_fname = fname.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
    let encode_lname = lname.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
    let encode_phone = phone.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
    
    let requestUrl = raw_requestUrl + "&fname=" + encode_fname + "&lname=" + encode_lname + "&phone=" + encode_phone
    
    print("=> Request: " + requestUrl)
    
    // POST method (Extras)
    let url = URL(string: requestUrl)!
    var request = URLRequest(url: url)
    request.httpMethod = "POST"
    
    let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
        guard let dataResponse = data,
            error == nil else {
                print(error?.localizedDescription ?? "Response Error")
                return }
        do{
            let jsonResponse = try JSONSerialization.jsonObject(with:
                dataResponse, options: [])
            //            print(jsonResponse) //Response result
            
            guard let jsonArray = jsonResponse as? [String: Any] else {
                return
            }
            //            print(jsonArray)
            
            guard let res = jsonArray["return"] as? Bool else { return }
            print(res)
            if(res == true){
                
                completionHandler(true)
            } else {
                
                completionHandler(false)
            }
            
        } catch let parsingError {
            print("Error", parsingError)
            completionHandler(false)
        }
    }
    
    task.resume()
}

// POST method creating new activity from user_id
// https://www.snickies.com/thompson/patient/add_activity
func requestAddActivity(pid: String, key: String, type: String, subtype: String, rating: String, rating_des: String, raw_notes: String, lat: String, long: String,completionHandler: @escaping (_ result: Bool) -> Void) {
    
    let raw_requestUrl = baseUrl + "/patient/add_activity?key=" + key + "&pid=" + pid + "&type=" + type + "&subtype=" + subtype + "&rating=" + rating + "&lat=" + lat + "&long=" + long
    let comment = raw_notes.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
    let rating_emo = rating_des.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
    
    let systemDate = Date();
    let localDate = systemDate.convertTo(region: Region.local)
    let date = localDate.toFormat("YYYY-MM-dd'T'HH:mm:ss.SSSZ")
    
    let requestUrl = raw_requestUrl + "&comment=" + comment + "&rating_des=" + rating_emo + "&date=" + date
    
    print("=> Request: " + requestUrl)
    
    // POST method (Extras)
    let url = URL(string: requestUrl)!
    var request = URLRequest(url: url)
    request.httpMethod = "POST"
    
    let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
        guard let dataResponse = data,
            error == nil else {
                print(error?.localizedDescription ?? "Response Error")
                return }
        do{
            let jsonResponse = try JSONSerialization.jsonObject(with:
                dataResponse, options: [])
            //            print(jsonResponse) //Response result
            
            guard let jsonArray = jsonResponse as? [String: Any] else {
                return
            }
            //            print(jsonArray)
            
            guard let res = jsonArray["return"] as? Bool else { return }
            print(res)
            if(res == true){
                
                completionHandler(true)
            } else {
                
                completionHandler(false)
            }
            
        } catch let parsingError {
            print("Error", parsingError)
            completionHandler(false)
        }
    }
    
    task.resume()
}

// POST method creating new internal record from user_id
// https://www.snickies.com/thompson/patient/add_sleep
func requestAddSleep(pid: String, key: String, duration: String, depth: String, interuption: String, snoring: String, time_to_sleep: String, time_to_up: String, raw_notes: String, lat: String, long: String,completionHandler: @escaping (_ result: Bool) -> Void) {
    
    let raw_requestUrl = baseUrl + "/patient/add_sleep?key=" + key + "&pid=" + pid + "&du=" + duration + "&inter=" + interuption + "&sn=" + snoring + "&tts=" + time_to_sleep + "&ttu=" + time_to_up + "&lat=" + lat + "&long=" + long
    let comment = raw_notes.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
    let depth_encode = depth.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
    
    let systemDate = Date();
    let localDate = systemDate.convertTo(region: Region.local)
    let date = localDate.toFormat("YYYY-MM-dd'T'HH:mm:ss.SSSZ")
    
    let requestUrl = raw_requestUrl + "&co=" + comment + "&de=" + depth_encode + "&date=" + date
    
    print("=> Request: " + requestUrl)
    
    // POST method (Extras)
    let url = URL(string: requestUrl)!
    var request = URLRequest(url: url)
    request.httpMethod = "POST"
    
    let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
        guard let dataResponse = data,
            error == nil else {
                print(error?.localizedDescription ?? "Response Error")
                return }
        do{
            let jsonResponse = try JSONSerialization.jsonObject(with:
                dataResponse, options: [])
            //            print(jsonResponse) //Response result
            
            guard let jsonArray = jsonResponse as? [String: Any] else {
                return
            }
            //            print(jsonArray)
            
            guard let res = jsonArray["return"] as? Bool else { return }
            print(res)
            if(res == true){
                
                completionHandler(true)
            } else {
                
                completionHandler(false)
            }
            
        } catch let parsingError {
            print("Error", parsingError)
            completionHandler(false)
        }
    }
    
    task.resume()
}

// POST method creating new internal record from user_id
// https://www.snickies.com/thompson/patient/add_medicine
func requestAddMedicine(pid: String, key: String, name: String, dosage: String, quantity: String, time_of_day: String, raw_notes: String, lat: String, long: String,completionHandler: @escaping (_ result: Bool) -> Void) {
    
    let raw_requestUrl = baseUrl + "/patient/add_medicine?key=" + key + "&pid=" + pid + "&qu=" + quantity + "&lat=" + lat + "&long=" + long
    let comment = raw_notes.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
    let encodename = name.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
    let encodedosage = dosage.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
    let encode_tod = time_of_day.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
    
    let systemDate = Date();
    let localDate = systemDate.convertTo(region: Region.local)
    let date = localDate.toFormat("YYYY-MM-dd'T'HH:mm:ss.SSSZ")
    
    let requestUrl = raw_requestUrl + "&st=" + encodedosage + "&na=" + encodename + "&co=" + comment + "&tod=" + encode_tod + "&date=" + date
    
    print("=> Request: " + requestUrl)
    
    // POST method (Extras)
    let url = URL(string: requestUrl)!
    var request = URLRequest(url: url)
    request.httpMethod = "POST"
    
    let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
        guard let dataResponse = data,
            error == nil else {
                print(error?.localizedDescription ?? "Response Error")
                return }
        do{
            let jsonResponse = try JSONSerialization.jsonObject(with:
                dataResponse, options: [])
            //            print(jsonResponse) //Response result
            
            guard let jsonArray = jsonResponse as? [String: Any] else {
                return
            }
            //            print(jsonArray)
            
            guard let res = jsonArray["return"] as? Bool else { return }
            print(res)
            if(res == true){
                completionHandler(true)
            } else {
                completionHandler(false)
            }
            
        } catch let parsingError {
            print("Error", parsingError)
            completionHandler(false)
        }
    }
    
    task.resume()
}
