
//
//  ProfileViewController.swift
//  Assists
//
//  Created by Thunpisit Amnuaikiatloet on 8/29/19.
//  Copyright © 2019 Thunpisit Amnuaikiatloet. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableview: UITableView!
    var categorySections = [Profile]()
    var patients: [Patient]? = []
    
    var firstname = UserDefaults.standard.string(forKey: "user_fname") ?? "null"
    var lastname = UserDefaults.standard.string(forKey: "user_lname") ?? "null"
    var role = UserDefaults.standard.string(forKey: "user_role") ?? "null"
    
    var patientId = UserDefaults.standard.string(forKey: "patient_id") ?? "null"
    var patientFirstname = UserDefaults.standard.string(forKey: "patient_fname") ?? "null"
    var patientLastname = UserDefaults.standard.string(forKey: "patient_lname") ?? "null"
    var patientNickname = UserDefaults.standard.string(forKey: "patient_nname") ?? "null"
    
    var userKey = UserDefaults.standard.string(forKey: "user_key") ?? "null"

    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var profileName: UILabel!
    @IBOutlet weak var profileRole: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = ""
        style()
        setupProfile()
    }
    
    func style() {
        // + CUSTOM NAVIGATION CONTROLLER
        // Change navigation controller back button to grey
        self.navigationController?.navigationBar.tintColor = UIColor(red:0.02, green:0.17, blue:0.36, alpha:1.0)
        // Change navigationContrller font to Avenir
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: "Avenir-Medium", size: 16)!]
        // Hide navigationController hairline
        self.navigationController?.navigationBar.setValue(true, forKey: "hidesShadow")
        self.navigationController?.navigationBar.isTranslucent = false
        
        // + CUSTOM VIEW CONTROLLER
        // Change background color
        self.view.backgroundColor = UIColor.white
    }
    
    func setupProfile() {
        // Person deail
        profileName.text = firstname.capitalized + " " + lastname.capitalized
        profileRole.text = "Role: " + role.capitalized
        
//        ["type":"Devices", "detail": "Connected to smart devices", "image": "device"]]
//        ["type":"Consent", "detail":"View & sign your consent", "image": "consent"],
        
//        ["type":"Privacy","detail": "Understand more about your patient data", "image": "privacy"],["type":"Term of service", "detail":"Understand what we do & provides", "image": "term"],["type":"FAQ", "detail": "Let us answer your questions", "image": "faq"],
        
        // List of items in profile
        categorySections = [Profile(name: "management", items: [["type":"Patient", "detail": "View: " + patientFirstname.capitalized + " " + patientLastname.capitalized, "image": "patient"], ["type": "Swtich patient", "detail": "Change to different patient", "image": "switch"]]),Profile(name: "Help & Privacy", items: [ ["type":"Help","detail": "Need human support? Reach out to us!", "image": "help"],["type":"Terms & Privacy","detail": "Terms of use + Privacy Policy", "image": "term"]]),Profile(name: "setting", items: [["type":"Logout", "detail": "Exiting of your account", "image": "logout"]])]
        
//        categorySections = [Profile(name: "management", items: [["type":"Personal", "detail": "View & edit your account", "image": "personal"],["type":"Patient", "detail": "View: " + patientFirstname.capitalized + " " + patientLastname.capitalized, "image": "patient"], ["type": "Swtich patient", "detail": "Change to different patient", "image": "switch"]]),Profile(name: "Help & Privacy", items: [ ["type":"Help","detail": "Need human support? Reach out to us!", "image": "help"]]),Profile(name: "setting", items: [["type":"About", "detail": "Information about this app", "image": "about"],["type":"Reset password", "detail": "Forgot your password?", "image": "reset"],["type":"Logout", "detail": "Exiting of your account", "image": "logout"]])]
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return self.categorySections.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return self.categorySections[section].name.capitalized
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let items = self.categorySections[section].items
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "profileCell") as! ProfileTableViewCell
            let items = self.categorySections[indexPath.section].items
            let item = items[indexPath.row]
    
            cell.itemName.text = item["type"]
            cell.itemDetail.text = item["detail"]
            cell.itemImage.image = UIImage.init(imageLiteralResourceName: String(item["image"]! + "Icon"))
            cell.accessoryType = .disclosureIndicator
    
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        guard let header = view as? UITableViewHeaderFooterView else { return }
        header.textLabel?.textColor = UIColor.black
        header.textLabel?.font = UIFont.init(name: "Avenir", size: 14)
        //        header.textLabel?.frame = header.frame
        header.textLabel?.frame = CGRect(x: 20, y: 8, width: 320, height: 25)
        header.textLabel?.textAlignment = .left
    }
    
    // Deselect row after touch cell
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableview.deselectRow(at: indexPath, animated: true)
        
        tableview.deselectRow(at: indexPath, animated: true)
        
        let items = self.categorySections[indexPath.section].items
        let item = items[indexPath.row]["image"]!
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        if item == "logout" {
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "signinNavigationController") as! UINavigationController
            removeUser()
            removePatient()
            nextViewController.modalPresentationStyle = .fullScreen
            self.present(nextViewController, animated: true)
        } else if item == "switch" {
            
            startLoading()
            
            // APIs request user information
            requestUser(key: userKey, completionHandler: { (user) in
                
                DispatchQueue.main.async(execute: { () -> Void in
                    
                    // For loop retrieving each patients information
                    for patient in user.patients {
                        requestPatient(key: self.userKey, pid: patient, completionHandler: { (child) in
                            self.patients?.append(child)
                        })
                    }
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                        let patientListViewController = storyBoard.instantiateViewController(withIdentifier: "patientListViewController") as! PatientListViewController
                        // Send patients information to PatientsViewController
                        patientListViewController.patients = self.patients
                        self.dismiss(animated: true, completion: nil)
                        self.present(patientListViewController, animated: true, completion: nil)
                    }
                })
            })
        } else if item == "term" {
            guard let url = URL(string: "https://www.autismassists.com/legal") else { return }
            UIApplication.shared.open(url)
        } else if item == "help" {
            // Get open email and set to help@autismassists.com to works!

        }
    }
    
    func startLoading() {
        let alert = UIAlertController(title: nil, message: "Loading...", preferredStyle: .alert)
        
        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = UIActivityIndicatorView.Style.gray
        loadingIndicator.startAnimating();
        
        alert.view.addSubview(loadingIndicator)
        present(alert, animated: true, completion: nil)
    }
    
    func removeUser() {
        UserDefaults.standard.removeObject(forKey: "user_key")
        UserDefaults.standard.removeObject(forKey: "user_id")
        UserDefaults.standard.removeObject(forKey: "user_fname")
        UserDefaults.standard.removeObject(forKey: "user_lname")
        UserDefaults.standard.removeObject(forKey: "user_role")
        UserDefaults.standard.removeObject(forKey: "user_email")
    }
    
    func removePatient() {
        UserDefaults.standard.removeObject(forKey: "patient_id")
        UserDefaults.standard.removeObject(forKey: "patient_fname")
        UserDefaults.standard.removeObject(forKey: "patient_lname")
        UserDefaults.standard.removeObject(forKey: "patient_nname")
        UserDefaults.standard.removeObject(forKey: "patient_insid")
    }

}
