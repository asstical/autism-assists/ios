//
//  WeekTableViewCell.swift
//  Assists
//
//  Created by Thunpisit Amnuaikiatloet on 9/19/19.
//  Copyright © 2019 Thunpisit Amnuaikiatloet. All rights reserved.
//

import UIKit
import Charts
import SwiftDate

class WeekTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

class WeekSleepTableViewCell: UITableViewCell {

    var events: [Event]? = []
    var sleeps: [Sleep]? = []
    
    @IBOutlet var weekSleepChart: BarChartView!
    @IBOutlet var weekSleepEtcChart: BarChartView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupWeekSleepChart() {
        
        let systemDate = Date();
        let localDate = systemDate.convertTo(region: Region.local)
        let todayDate = localDate.toFormat("YYYY-MM-dd")
    
        var sleepEntry: [BarChartDataEntry] = []
        var sleepTTSEntry: [BarChartDataEntry] = []
        var sleepTTWEntry: [BarChartDataEntry] = []
        
        // Data population
        for n in 1...7 {
                    
            let focusDate = todayDate.toDate()! - 7.days + n.days
            var focusDateCheck = false

            for e in events! {
    //                print("Focus Date:" + "\(focusDate.toFormat("YYYY-MM-dd"))")
    //                print("System Date:" + e.date)
                if focusDate.toFormat("YYYY-MM-dd") == e.date {
                    
                    focusDateCheck = true
                    
                    for s in e.sleeps {
                        sleepEntry.append(BarChartDataEntry(x: Double(n), y: Double(s.duration)!))
                        sleepTTSEntry.append(BarChartDataEntry(x: Double(n)*2, y: Double(s.timeToSleep)!))
                        sleepTTWEntry.append(BarChartDataEntry(x: (Double(n)*2)+1, y: Double(s.timeToWake)!))
                    }
                }
            }
            
            if focusDateCheck == false {
                sleepEntry.append(BarChartDataEntry(x: Double(n), y: 0))
                sleepTTSEntry.append(BarChartDataEntry(x: Double(n)*2, y: 0))
                sleepTTWEntry.append(BarChartDataEntry(x: (Double(n)*2)+1, y: 0))
            }
        }
        
        let sleepDataSet = BarChartDataSet(entries: sleepEntry, label: "Sleep duration (Hours)")
        let sleepTTSDataSet = BarChartDataSet(entries: sleepTTSEntry, label: "Time to sleep (Mins)")
        let sleepTTWDataSet = BarChartDataSet(entries: sleepTTWEntry, label: "Time to wake (Mins)")
        
        sleepDataSet.setColor(UIColor(red:0.00, green:0.80, blue:1.00, alpha:1.0))
        sleepTTWDataSet.setColor(UIColor(red:0.92, green:0.92, blue:0.92, alpha:1.0))
        sleepTTSDataSet.setColor(UIColor(red:0.47, green:0.53, blue:0.62, alpha:1.0))
        
        let chartData = BarChartData()
        let chartTTData = BarChartData()
        
        chartData.barWidth = 0.3
        chartData.setDrawValues(false)
        chartData.addDataSet(sleepDataSet)
        
        chartTTData.barWidth = 0.3
        chartTTData.setDrawValues(false)
        chartTTData.addDataSet(sleepTTSDataSet)
        chartTTData.addDataSet(sleepTTWDataSet)
        
        weekSleepChart.noDataText = "No Data"
        weekSleepChart.noDataFont = UIFont(name: "Avenir-Medium", size: 12)!
        weekSleepChart.noDataTextColor = UIColor.lightGray
        weekSleepChart.animate(xAxisDuration: 2, yAxisDuration: 2)
        
        weekSleepEtcChart.noDataText = "No Data"
        weekSleepEtcChart.noDataFont = UIFont(name: "Avenir-Medium", size: 12)!
        weekSleepEtcChart.noDataTextColor = UIColor.lightGray
        weekSleepEtcChart.animate(xAxisDuration: 2, yAxisDuration: 2)
        
        weekSleepChart.rightAxis.enabled = false
        weekSleepChart.rightAxis.drawGridLinesEnabled = false
        weekSleepChart.leftAxis.enabled = false
        weekSleepChart.leftAxis.drawGridLinesEnabled = false
        weekSleepChart.xAxis.enabled = false
        weekSleepChart.xAxis.drawGridLinesEnabled = false
        weekSleepChart.drawGridBackgroundEnabled = false
        weekSleepChart.isUserInteractionEnabled = false
        weekSleepChart.chartDescription?.enabled = false
        weekSleepChart.data = chartData
        
        weekSleepEtcChart.rightAxis.enabled = false
        weekSleepEtcChart.rightAxis.drawGridLinesEnabled = false
        weekSleepEtcChart.leftAxis.enabled = false
        weekSleepEtcChart.leftAxis.drawGridLinesEnabled = false
        weekSleepEtcChart.xAxis.enabled = false
        weekSleepEtcChart.xAxis.drawGridLinesEnabled = false
        weekSleepEtcChart.drawGridBackgroundEnabled = false
        weekSleepEtcChart.isUserInteractionEnabled = false
        weekSleepEtcChart.chartDescription?.enabled = true
        weekSleepEtcChart.data = chartTTData
    }

}

class WeekActivityTableViewCell: UITableViewCell {
    
    var events: [Event]? = []
    var activities: [Activity]? = []
    
    @IBOutlet var weekActivityChart: LineChartView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupWeekActivityChart() {
        
        let systemDate = Date();
        let localDate = systemDate.convertTo(region: Region.local)
        let todayDate = localDate.toFormat("YYYY-MM-dd")
        
        var medicalEntry: [ChartDataEntry] = []
        var emotionEntry: [ChartDataEntry] = []
        var behaviorEntry: [ChartDataEntry] = []
        
        for n in 1...7 {
            
            let focusDate = todayDate.toDate()! - 7.days + n.days
            var focusDateCheck = false
            
            for e in events! {

                if focusDate.toFormat("YYYY-MM-dd") == e.date {
                    
                    focusDateCheck = true
                    
                    var medicalCount = 0
                    var emotionCount = 0
                    var behaviorCount = 0
                    
                    for a in e.activities {
                        if a.type == "medical" {
                            medicalCount = medicalCount + 1
                        } else if a.type == "emotion" {
                            emotionCount = emotionCount + 1
                        } else if a.type == "behavior" {
                            behaviorCount = behaviorCount + 1
                        }
                    }
                    
                    medicalEntry.append(ChartDataEntry(x: Double(n), y: Double(medicalCount)))
                    emotionEntry.append(ChartDataEntry(x: Double(n), y: Double(emotionCount)))
                    behaviorEntry.append(ChartDataEntry(x: Double(n), y: Double(behaviorCount)))
                }
            }
            
            if focusDateCheck == false {
                medicalEntry.append(ChartDataEntry(x: Double(n), y: Double(0)))
                emotionEntry.append(ChartDataEntry(x: Double(n), y: Double(0)))
                behaviorEntry.append(ChartDataEntry(x: Double(n), y: Double(0)))
            }
        }
        
        let medicalDataSet = LineChartDataSet(entries: medicalEntry, label: "Medical")
        let emotionDataSet = LineChartDataSet(entries: emotionEntry, label: "Emotion")
        let behaviorDataSet = LineChartDataSet(entries: behaviorEntry, label: "Behavior")
        
        let chartData = LineChartData()
        chartData.setDrawValues(false)
        
        chartData.addDataSet(medicalDataSet)
        chartData.addDataSet(emotionDataSet)
        chartData.addDataSet(behaviorDataSet)
        
        medicalDataSet.colors = [UIColor(red:0.24, green:0.37, blue:0.91, alpha:1.0)]
        emotionDataSet.colors = [UIColor(red:0.26, green:0.89, blue:0.67, alpha:1.0)]
        behaviorDataSet.colors = [UIColor(red:0.94, green:0.35, blue:0.18, alpha:1.0)]
        medicalDataSet.drawCirclesEnabled = false
        emotionDataSet.drawCirclesEnabled = false
        behaviorDataSet.drawCirclesEnabled = false
        
        weekActivityChart.noDataText = "No Data"
        weekActivityChart.noDataFont = UIFont(name: "Avenir-Medium", size: 12)!
        weekActivityChart.noDataTextColor = UIColor.lightGray
        weekActivityChart.animate(xAxisDuration: 2, yAxisDuration: 2)
        
        weekActivityChart.rightAxis.enabled = false
        weekActivityChart.rightAxis.drawGridLinesEnabled = false
        weekActivityChart.leftAxis.enabled = false
        weekActivityChart.leftAxis.drawGridLinesEnabled = false
        weekActivityChart.xAxis.enabled = false
        weekActivityChart.xAxis.drawGridLinesEnabled = false
        weekActivityChart.drawGridBackgroundEnabled = false
        weekActivityChart.isUserInteractionEnabled = false
        weekActivityChart.chartDescription?.enabled = false
        weekActivityChart.data = chartData
    }

}

class WeekMedicineTableViewCell: UITableViewCell {
    
    var events: [Event]? = []
    var medicines: [Medicine]? = []
    
    @IBOutlet var weekMedicineChart: BarChartView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupWeekMedicineChart() {
    
        let systemDate = Date();
        let localDate = systemDate.convertTo(region: Region.local)
        let todayDate = localDate.toFormat("YYYY-MM-dd")
    
        var medicineEntry: [BarChartDataEntry] = []
        
        print("TODAY")
        print(todayDate)
        
        for n in 1...7 {
                    
            let focusDate = todayDate.toDate()! - 7.days + n.days
            var focusDateCheck = false
            
            print(focusDate)

            for e in events! {
                if focusDate.toFormat("YYYY-MM-dd") == e.date {
                    
                    focusDateCheck = true
                    medicineEntry.append(BarChartDataEntry(x: Double(n), y: Double(e.medicines.count)))
                }
            }
            
            if focusDateCheck == false {
                medicineEntry.append(BarChartDataEntry(x: Double(n), y: 0))
            }
        }
        
        let medicineDataSet = BarChartDataSet(entries: medicineEntry, label: "Number of medicine(s)")
        medicineDataSet.setColor(UIColor(red:0.02, green:0.17, blue:0.36, alpha:1.0))
        let chartData = BarChartData()
        
        chartData.barWidth = 0.3
        chartData.setDrawValues(false)
        chartData.addDataSet(medicineDataSet)
        
        weekMedicineChart.noDataText = "No Data"
        weekMedicineChart.noDataFont = UIFont(name: "Avenir-Medium", size: 12)!
        weekMedicineChart.noDataTextColor = UIColor.lightGray
        weekMedicineChart.animate(xAxisDuration: 2, yAxisDuration: 2)
        
        weekMedicineChart.rightAxis.enabled = false
        weekMedicineChart.rightAxis.drawGridLinesEnabled = false
        weekMedicineChart.leftAxis.enabled = false
        weekMedicineChart.leftAxis.drawGridLinesEnabled = false
        weekMedicineChart.xAxis.enabled = false
        weekMedicineChart.xAxis.drawGridLinesEnabled = false
        weekMedicineChart.drawGridBackgroundEnabled = false
        weekMedicineChart.isUserInteractionEnabled = false
        weekMedicineChart.chartDescription?.enabled = true
        weekMedicineChart.data = chartData
    }

}


