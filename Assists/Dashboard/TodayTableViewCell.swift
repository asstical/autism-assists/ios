//
//  TodayTableViewCell.swift
//  Assists
//
//  Created by Thunpisit Amnuaikiatloet on 10/1/19.
//  Copyright © 2019 Thunpisit Amnuaikiatloet. All rights reserved.
//

import UIKit
import Charts
import SwiftDate

class TodayTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

class TodaySleepTableViewCell: UITableViewCell {

    var events: [Event]? = []
    var sleeps: [Sleep]? = []
    
    @IBOutlet var todaySleepChart: PieChartView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupTodaySleepChart() {
        
        let systemDate = Date();
        let localDate = systemDate.convertTo(region: Region.local)
        let todayDate = localDate.toFormat("YYYY-MM-dd")
        
        var sleepDuration: Double?
        var timeToWake: Double?
        var timeToSleep: Double?
        
        var sleepDataEntry:[PieChartDataEntry] = []
                    
        let focusDate = todayDate.toDate()!
        var focusDateCheck = false

        for e in events! {
//                print("Focus Date:" + "\(focusDate.toFormat("YYYY-MM-dd"))")
//                print("System Date:" + e.date)
            if focusDate.toFormat("YYYY-MM-dd") == e.date {
                
                focusDateCheck = true
                
                for s in e.sleeps {
                    sleepDuration = Double(s.duration)
                    timeToWake = Double(s.timeToWake)
                    timeToSleep = Double(s.timeToSleep)
                }
            }
        }
        
        if focusDateCheck == false {
            sleepDuration = 0.0
            timeToWake = 0.0
            timeToSleep = 0.0
        }
        
        sleepDataEntry.append(PieChartDataEntry(value: timeToSleep!))
        sleepDataEntry.append(PieChartDataEntry(value: sleepDuration! * 60.0))
        sleepDataEntry.append(PieChartDataEntry(value: timeToWake!))
        
        let sleepDataSet = PieChartDataSet(entries: sleepDataEntry, label: "")
        let sleepData = PieChartData()
        
        sleepData.setDrawValues(false)
        sleepData.addDataSet(sleepDataSet)
        sleepDataSet.colors = [UIColor(red:0.24, green:0.37, blue:0.91, alpha:1.0),UIColor(red:0.26, green:0.89, blue:0.67, alpha:1.0),UIColor(red:0.94, green:0.35, blue:0.18, alpha:1.0)]
        
        todaySleepChart.noDataText = "No Data"
        todaySleepChart.noDataFont = UIFont(name: "Avenir-Medium", size: 12)!
        todaySleepChart.noDataTextColor = UIColor.lightGray
        todaySleepChart.animate(xAxisDuration: 2, yAxisDuration: 2)
        todaySleepChart.maxAngle = 180 // Half chart
        todaySleepChart.rotationAngle = 180
        
        todaySleepChart.animate(xAxisDuration: 1.4, easingOption: .easeOutBack)
        todaySleepChart.legend.enabled = false
        todaySleepChart.chartDescription?.enabled = false
        todaySleepChart.isUserInteractionEnabled = false
        todaySleepChart.data = sleepData
    }

}
