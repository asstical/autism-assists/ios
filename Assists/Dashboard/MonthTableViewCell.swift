//
//  MonthTableViewCell.swift
//  Assists
//
//  Created by Thunpisit Amnuaikiatloet on 9/19/19.
//  Copyright © 2019 Thunpisit Amnuaikiatloet. All rights reserved.
//

import UIKit
import Charts
import SwiftDate

class MonthTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

class MonthSleepTableViewCell: UITableViewCell {

    var events: [Event]? = []
    var sleeps: [Sleep]? = []
    
    @IBOutlet var monthSleepChart: LineChartView!
    @IBOutlet var monthSleepEtcChart: LineChartView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupMonthSleepChart() {
        
        let systemDate = Date();
        let localDate = systemDate.convertTo(region: Region.local)
        let todayDate = localDate.toFormat("YYYY-MM-dd")
    
        var sleepEntry: [ChartDataEntry] = []
        var sleepTTSEntry: [ChartDataEntry] = []
        var sleepTTWEntry: [ChartDataEntry] = []
        
        // Data population
        for n in 1...30 {
                    
            let focusDate = todayDate.toDate()! - 30.days + n.days
            var focusDateCheck = false

            for e in events! {
    //                print("Focus Date:" + "\(focusDate.toFormat("YYYY-MM-dd"))")
    //                print("System Date:" + e.date)
                if focusDate.toFormat("YYYY-MM-dd") == e.date {
                    
                    focusDateCheck = true
                    
                    for s in e.sleeps {
                        sleepEntry.append(ChartDataEntry(x: Double(n), y: Double(s.duration)!))
                        sleepTTSEntry.append(ChartDataEntry(x: Double(n), y: Double(s.timeToSleep)!))
                        sleepTTWEntry.append(ChartDataEntry(x: Double(n), y: Double(s.timeToWake)!))
                    }
                }
            }
            
            if focusDateCheck == false {
                sleepEntry.append(ChartDataEntry(x: Double(n), y: 0))
                sleepTTSEntry.append(ChartDataEntry(x: Double(n), y: 0))
                sleepTTWEntry.append(ChartDataEntry(x: Double(n), y: 0))
            }
        }
        
        let sleepDataSet = LineChartDataSet(entries: sleepEntry, label: "Sleep duration (Hours)")
        let sleepTTSDataSet = LineChartDataSet(entries: sleepTTSEntry, label: "Time to sleep (Mins)")
        let sleepTTWDataSet = LineChartDataSet(entries: sleepTTWEntry, label: "Time to wake (Mins)")
        
        sleepDataSet.setColor(UIColor(red:0.00, green:0.80, blue:1.00, alpha:1.0))
        sleepDataSet.drawCirclesEnabled = false
        sleepDataSet.drawValuesEnabled = false
        
        
        // UIColor(red:0.02, green:0.17, blue:0.36, alpha:1.0)
        // UIColor(red:0.92, green:0.92, blue:0.92, alpha:1.0)
        
        sleepTTWDataSet.setColor(UIColor(red:0.92, green:0.92, blue:0.92, alpha:1.0))
        sleepTTWDataSet.drawCirclesEnabled = false
        sleepTTWDataSet.drawValuesEnabled = false
        
        sleepTTSDataSet.setColor(UIColor(red:0.47, green:0.53, blue:0.62, alpha:1.0))
        sleepTTSDataSet.drawCirclesEnabled = false
        sleepTTSDataSet.drawValuesEnabled = false
        
        let chartData = LineChartData()
        chartData.setDrawValues(false)
        chartData.addDataSet(sleepDataSet)
        
        let chartTTData = LineChartData()
        chartTTData.setDrawValues(false)
        chartTTData.addDataSet(sleepTTSDataSet)
        chartTTData.addDataSet(sleepTTWDataSet)
        
        monthSleepChart.noDataText = "No Data"
        monthSleepChart.noDataFont = UIFont(name: "Avenir-Medium", size: 12)!
        monthSleepChart.noDataTextColor = UIColor.lightGray
        monthSleepChart.animate(xAxisDuration: 2, yAxisDuration: 2)
        
        monthSleepEtcChart.noDataText = "No Data"
        monthSleepEtcChart.noDataFont = UIFont(name: "Avenir-Medium", size: 12)!
        monthSleepEtcChart.noDataTextColor = UIColor.lightGray
        monthSleepEtcChart.animate(xAxisDuration: 2, yAxisDuration: 2)
        
        monthSleepChart.rightAxis.enabled = true
        monthSleepChart.rightAxis.drawGridLinesEnabled = false
        monthSleepChart.leftAxis.enabled = true
        monthSleepChart.leftAxis.drawGridLinesEnabled = false
        monthSleepChart.xAxis.enabled = false
        monthSleepChart.xAxis.drawGridLinesEnabled = false
        monthSleepChart.drawGridBackgroundEnabled = false
        monthSleepChart.isUserInteractionEnabled = false
        monthSleepChart.chartDescription?.enabled = true
        monthSleepChart.data = chartData
        
        monthSleepEtcChart.rightAxis.enabled = true
        monthSleepEtcChart.rightAxis.drawGridLinesEnabled = false
        monthSleepEtcChart.leftAxis.enabled = true
        monthSleepEtcChart.leftAxis.drawGridLinesEnabled = false
        monthSleepEtcChart.xAxis.enabled = false
        monthSleepEtcChart.xAxis.drawGridLinesEnabled = false
        monthSleepEtcChart.drawGridBackgroundEnabled = false
        monthSleepEtcChart.isUserInteractionEnabled = false
        monthSleepEtcChart.chartDescription?.enabled = true
        monthSleepEtcChart.data = chartTTData
    }

}

class MonthActivityTableViewCell: UITableViewCell {
    
    var events: [Event]? = []
    var activities: [Activity]? = []
    
    @IBOutlet weak var monthActivityChart: LineChartView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupMonthActivityChart() {
        
        let systemDate = Date();
        let localDate = systemDate.convertTo(region: Region.local)
        let todayDate = localDate.toFormat("YYYY-MM-dd")
        
        var medicalEntry: [ChartDataEntry] = []
        var emotionEntry: [ChartDataEntry] = []
        var behaviorEntry: [ChartDataEntry] = []
        
        for n in 1...30 {
            
            let focusDate = todayDate.toDate()! - 30.days + n.days
            var focusDateCheck = false
            
            for e in events! {

                if focusDate.toFormat("YYYY-MM-dd") == e.date {
                    
                    focusDateCheck = true
                    
                    var medicalCount = 0
                    var emotionCount = 0
                    var behaviorCount = 0
                    
                    for a in e.activities {
                        if a.type == "medical" {
                            medicalCount = medicalCount + 1
                        } else if a.type == "emotion" {
                            emotionCount = emotionCount + 1
                        } else if a.type == "behavior" {
                            behaviorCount = behaviorCount + 1
                        }
                    }
                    
                    medicalEntry.append(ChartDataEntry(x: Double(n), y: Double(medicalCount)))
                    emotionEntry.append(ChartDataEntry(x: Double(n), y: Double(emotionCount)))
                    behaviorEntry.append(ChartDataEntry(x: Double(n), y: Double(behaviorCount)))
                }
            }
            
            if focusDateCheck == false {
                medicalEntry.append(ChartDataEntry(x: Double(n), y: Double(0)))
                emotionEntry.append(ChartDataEntry(x: Double(n), y: Double(0)))
                behaviorEntry.append(ChartDataEntry(x: Double(n), y: Double(0)))
            }
        }
        
        let medicalDataSet = LineChartDataSet(entries: medicalEntry, label: "Medical")
        let emotionDataSet = LineChartDataSet(entries: emotionEntry, label: "Emotion")
        let behaviorDataSet = LineChartDataSet(entries: behaviorEntry, label: "Behavior")
        medicalDataSet.drawValuesEnabled = false
        emotionDataSet.drawValuesEnabled = false
        behaviorDataSet.drawValuesEnabled = false
        
        let chartData = LineChartData()
        chartData.setDrawValues(false)
        
        chartData.addDataSet(medicalDataSet)
        chartData.addDataSet(emotionDataSet)
        chartData.addDataSet(behaviorDataSet)
        
        medicalDataSet.colors = [UIColor(red:0.24, green:0.37, blue:0.91, alpha:1.0)]
        emotionDataSet.colors = [UIColor(red:0.26, green:0.89, blue:0.67, alpha:1.0)]
        behaviorDataSet.colors = [UIColor(red:0.94, green:0.35, blue:0.18, alpha:1.0)]
        medicalDataSet.drawCirclesEnabled = false
        emotionDataSet.drawCirclesEnabled = false
        behaviorDataSet.drawCirclesEnabled = false
        
        monthActivityChart.noDataText = "No Data"
        monthActivityChart.noDataFont = UIFont(name: "Avenir-Medium", size: 12)!
        monthActivityChart.noDataTextColor = UIColor.lightGray
        monthActivityChart.animate(xAxisDuration: 2, yAxisDuration: 2)
        
        monthActivityChart.rightAxis.enabled = true
        monthActivityChart.rightAxis.drawGridLinesEnabled = false
        monthActivityChart.leftAxis.enabled = true
        monthActivityChart.leftAxis.drawGridLinesEnabled = false
        monthActivityChart.xAxis.enabled = false
        monthActivityChart.xAxis.drawGridLinesEnabled = false
        monthActivityChart.drawGridBackgroundEnabled = false
        monthActivityChart.isUserInteractionEnabled = false
        monthActivityChart.chartDescription?.enabled = false
        monthActivityChart.data = chartData
    }

}

class MonthMedicineTableViewCell: UITableViewCell {
    
    var events: [Event]? = []
    var medicines: [Medicine]? = []
    
    @IBOutlet weak var monthMedicineChart: BarChartView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupMonthMedicineChart() {
    
        let systemDate = Date();
        let localDate = systemDate.convertTo(region: Region.local)
        let todayDate = localDate.toFormat("YYYY-MM-dd")
    
        var medicineEntry: [BarChartDataEntry] = []
        
        print("TODAY")
        print(todayDate)
        
        for n in 1...30 {
                    
            let focusDate = todayDate.toDate()! - 30.days + n.days
            var focusDateCheck = false
            
            print(focusDate)

            for e in events! {
                if focusDate.toFormat("YYYY-MM-dd") == e.date {
                    
                    focusDateCheck = true
                    medicineEntry.append(BarChartDataEntry(x: Double(n), y: Double(e.medicines.count)))
                }
            }
            
            if focusDateCheck == false {
                medicineEntry.append(BarChartDataEntry(x: Double(n), y: 0))
            }
        }
        
        let medicineDataSet = BarChartDataSet(entries: medicineEntry, label: "Number of medicine(s)")
        medicineDataSet.setColor(UIColor(red:0.02, green:0.17, blue:0.36, alpha:1.0))
        medicineDataSet.drawValuesEnabled = false
        
        let chartData = BarChartData()
        chartData.barWidth = 0.3
        chartData.setDrawValues(false)
        chartData.addDataSet(medicineDataSet)
        
        monthMedicineChart.noDataText = "No Data"
        monthMedicineChart.noDataFont = UIFont(name: "Avenir-Medium", size: 12)!
        monthMedicineChart.noDataTextColor = UIColor.lightGray
        monthMedicineChart.animate(xAxisDuration: 2, yAxisDuration: 2)
        
        monthMedicineChart.rightAxis.enabled = true
        monthMedicineChart.rightAxis.drawGridLinesEnabled = false
        monthMedicineChart.leftAxis.enabled = true
        monthMedicineChart.leftAxis.drawGridLinesEnabled = false
        monthMedicineChart.xAxis.enabled = false
        monthMedicineChart.xAxis.drawGridLinesEnabled = false
        monthMedicineChart.drawGridBackgroundEnabled = false
        monthMedicineChart.isUserInteractionEnabled = false
        monthMedicineChart.chartDescription?.enabled = true
        monthMedicineChart.data = chartData
    }

}
