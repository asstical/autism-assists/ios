//
//  YearViewController.swift
//  Assists
//
//  Created by Thunpisit Amnuaikiatloet on 9/4/19.
//  Copyright © 2019 Thunpisit Amnuaikiatloet. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class YearViewController: UIViewController, IndicatorInfoProvider {

    override func viewDidLoad() {
        super.viewDidLoad()

        style()
    }
    
    // XLPagerTabStrip: Sending name of viewController to DashboardViewController
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "Year")
    }
    
    func style() {
        // + CUSTOM VIEW CONTROLLER
        // Change background color
//        self.view.backgroundColor = UIColor(red:0.93, green:0.93, blue:0.93, alpha:1.0)
        self.view.backgroundColor = UIColor.blue
    }

}
