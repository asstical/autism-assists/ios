//
//  WeekViewController.swift
//  Assists
//
//  Created by Thunpisit Amnuaikiatloet on 9/4/19.
//  Copyright © 2019 Thunpisit Amnuaikiatloet. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class WeekViewController: UIViewController, IndicatorInfoProvider, UITableViewDelegate, UITableViewDataSource {
    
    var events: [Event]? = []

    var patientId = UserDefaults.standard.string(forKey: "patient_id") ?? "null"
    var userKey = UserDefaults.standard.string(forKey: "user_key") ?? "null"
    
    @IBOutlet weak var tableview: UITableView!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)

        updateData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        style()
        
        DispatchQueue.main.async {
//            self.startLoading()
            self.updateData()
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                self.tableview.reloadData()
//                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    // XLPagerTabStrip: Sending name of viewController to DashboardViewController
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "Week")
    }
    
    func style() {
        // + CUSTOM VIEW CONTROLLER
        // Change background color
//        self.view.backgroundColor = UIColor(red:0.93, green:0.93, blue:0.93, alpha:1.0)
        self.view.backgroundColor = UIColor.red
    }
    
    func updateData(){
        DispatchQueue.main.async {
            requestEvents(pid: self.patientId, key: self.userKey, completionHandler: { (result) in
                self.events = result
            })
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                self.tableview.reloadData()
            }
        }
    }
    
    func startLoading() {
        let alert = UIAlertController(title: nil, message: "Loading...", preferredStyle: .alert)
        
        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = UIActivityIndicatorView.Style.gray
        loadingIndicator.startAnimating();
        
        alert.view.addSubview(loadingIndicator)
        present(alert, animated: true, completion: nil)
    }
    
    // UITABLEVIEWCONTROLLER SECTION //
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "weekSleepCell") as! WeekSleepTableViewCell
            cell.selectionStyle = .none
            cell.weekSleepChart.data = nil
            cell.events = events
            cell.setupWeekSleepChart()
            return cell
        }
        else if indexPath.row == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "weekActivityCell") as! WeekActivityTableViewCell
            cell.selectionStyle = .none
            cell.weekActivityChart.data = nil
            cell.events = events
            cell.setupWeekActivityChart()
            return cell
        }
        else if indexPath.row == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "weekMedicineCell") as! WeekMedicineTableViewCell
            cell.selectionStyle = .none
            cell.weekMedicineChart.data = nil
            cell.events = events
            cell.setupWeekMedicineChart()
            return cell
        }
        else {
            let cell: UITableViewCell = UITableViewCell(style: .default, reuseIdentifier: "weekMedicineCell")
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableview.deselectRow(at: indexPath, animated: true)
    }

}


