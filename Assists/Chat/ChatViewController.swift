//
//  ChatViewController.swift
//  Assists
//
//  Created by Thunpisit Amnuaikiatloet on 8/29/19.
//  Copyright © 2019 Thunpisit Amnuaikiatloet. All rights reserved.
//

import UIKit

class ChatViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        style()
    }
    
    func style() {
        // + CUSTOM NAVIGATION CONTROLLER
        // Change navigation controller back button to grey
        self.navigationController?.navigationBar.tintColor = UIColor(red:0.02, green:0.17, blue:0.36, alpha:1.0)
        // Change navigationContrller font to Avenir
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: "Avenir-Medium", size: 16)!]
        
        // + CUSTOM VIEW CONTROLLER
        // Change background color
//        self.view.backgroundColor = UIColor(red:0.93, green:0.93, blue:0.93, alpha:1.0)
        self.view.backgroundColor = UIColor.white
    }

}
