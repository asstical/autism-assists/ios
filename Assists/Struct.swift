//
//  Struct.swift
//  Assists
//
//  Created by Thunpisit Amnuaikiatloet on 9/4/19.
//  Copyright © 2019 Thunpisit Amnuaikiatloet. All rights reserved.
//

import Foundation

struct User {
    var id: String
    var role: String
    var active: Bool
    var consent: Bool
    var email: String
    var key: String
    var firstname: String
    var lastname: String
    var phone: String
    var patients: [String]
    
    init(id: String, role: String, active: Bool, consent: Bool, email: String, key: String, firstname: String, lastname: String, phone: String, patients: [String]) {
        self.id = id
        self.role = role
        self.active = active
        self.consent = consent
        self.email = email
        self.key = key
        self.firstname = firstname
        self.lastname = lastname
        self.phone = phone
        self.patients = patients
    }
}

struct Patient {
    var id: String
    var active: Bool
    var consent: Bool
    var firstname: String
    var lastname: String
    var nickname: String
    var dob: String
    var gender: String
    var blood: String
    var institutionId: String
    var institution: String
    var doctor: String
//    var events: [Event]
    
    init(id: String, active: Bool, consent: Bool, firstname: String, lastname: String, nickname: String, dob: String, gender: String, blood: String, institutionId: String, institution: String, doctor: String) {
        
        self.id = id
        self.active = active
        self.consent = consent
        self.firstname = firstname
        self.lastname = lastname
        self.nickname = nickname
        self.dob = dob
        self.gender = gender
        self.blood = blood
        self.institutionId = institutionId
        self.institution = institution
        self.doctor = doctor
//        self.events = [Event]()
    }
}

struct Event {
    var date: String
    var activities: [Activity]
    var sleeps: [Sleep]
    var medicines: [Medicine]
    
    init(date: String) {
        self.date = date
        self.activities = [Activity]()
        self.sleeps = [Sleep]()
        self.medicines = [Medicine]()
    }
}

struct Activity {
    var date: String
    var type: String
    var subtype: String
    var rating: String
    var description: String
    var notes: String
    
    init(date: String, type: String, subtype: String, rating: String, description: String, notes: String) {
        self.date = date
        self.type = type
        self.subtype = subtype
        self.rating = rating
        self.description = description
        self.notes = notes
    }
}

struct Sleep {
    var date: String
    var duration: String
    var depth: String
    var interuption: String
    var snoring: String
    var timeToSleep: String
    var timeToWake: String
    var notes: String
    
    init(date: String, duration: String, depth: String, interuption: String, snoring: String, timeToSleep: String, timeToWake: String, notes: String) {
        self.date = date
        self.duration = duration
        self.depth = depth
        self.interuption = interuption
        self.snoring = snoring
        self.timeToSleep = timeToSleep
        self.timeToWake = timeToWake
        self.notes = notes
    }
}

struct Medicine {
    var date: String
    var name: String
    var type: String
    var dosage: String
    var quantity: String
    var timeTaken: String
    var notes: String
    
    init(date: String, name: String, type: String, dosage: String, quantity: String, timeTaken: String, notes: String) {
        self.date = date
        self.name = name
        self.type = type
        self.dosage = dosage
        self.quantity = quantity
        self.timeTaken = timeTaken
        self.notes = notes
    }
}

struct AddCategory {
    let name: String
    var items: [[String:String]]
}

struct Profile {
    let name: String
    var items: [[String:String]]
}
