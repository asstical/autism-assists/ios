//
//  SignupViewController.swift
//  Assists
//
//  Created by Thunpisit Amnuaikiatloet on 8/27/19.
//  Copyright © 2019 Thunpisit Amnuaikiatloet. All rights reserved.
//

import UIKit
import Loaf
import Eureka

class SignupViewController: FormViewController {
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
//        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
//        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Sign up"
        style()
        setupForm()
    }
    
    func style() {
        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        } else {
            // Fallback on earlier versions
        }  
        // + CUSTOM NAVIGATION CONTROLLER
        // Change navigation controller button to grey
        self.navigationController?.navigationBar.tintColor = UIColor(red:0.02, green:0.17, blue:0.36, alpha:1.0)
        // Change navigation controller bar to white
        self.navigationController?.navigationBar.barTintColor = UIColor.white
        // Change navigationContrller font to Avenir
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: "Avenir-Medium", size: 16)!]
        // Hide navigationController hairline
//        self.navigationController?.navigationBar.setValue(true, forKey: "hidesShadow")
//        self.navigationController?.navigationBar.isTranslucent = false
        
        // + CUSTOM VIEW CONTROLLER
        // Change background color
        self.view.backgroundColor = UIColor.white
    }
    
    // Form using Eureka framework with validation
    func setupForm(){
        
        LabelRow.defaultCellUpdate = { cell, row in
            cell.contentView.backgroundColor = .red
            cell.textLabel?.textColor = .white
            cell.textLabel?.font = UIFont.init(name: "Avenir", size: 14)
            cell.textLabel?.textAlignment = .right
            
        }
        
        TextRow.defaultCellUpdate = { cell, row in
            if !row.isValid {
                cell.titleLabel?.textColor = .red
            }
        }
        
        form
            // Credential section
            +++ Section(header: "Credential", footer: "Invite code must be requested")
            // Email with validation
            <<< EmailRow() {
                $0.title = "Email"
                $0.tag = "email"
                $0.placeholder = "your@email.com"
                $0.add(rule: RuleRequired())
                $0.add(rule: RuleEmail())
                $0.validationOptions = .validatesOnChange
                }.cellUpdate { cell, row in
                    if !row.isValid {
                        cell.titleLabel?.textColor = .red
                    }
                    cell.textLabel?.font = UIFont.init(name: "Avenir", size: 14)
                    cell.textField?.font = UIFont.init(name: "Avenir", size: 14)
                    cell.tintColor = UIColor(red:0.24, green:0.37, blue:0.91, alpha:1.0)
            }
            <<< PasswordRow() {
                $0.title = "Password"
                $0.tag = "password"
                $0.placeholder = ""
                $0.add(rule: RuleRequired())
                $0.validationOptions = .validatesOnChange
                }.cellUpdate { cell, row in
                    if !row.isValid {
                        cell.titleLabel?.textColor = .red
                    }
                    cell.textLabel?.font = UIFont.init(name: "Avenir", size: 14)
                    cell.textField?.font = UIFont.init(name: "Avenir", size: 14)
                    cell.tintColor = UIColor(red:0.24, green:0.37, blue:0.91, alpha:1.0)
            }
            <<< TextRow() {
                $0.title = "Invite code"
                $0.tag = "code"
                $0.placeholder = "j0Vsk$%"
                $0.add(rule: RuleRequired())
                $0.validationOptions = .validatesOnChange
                }.cellUpdate { cell, row in
                    if !row.isValid {
                        cell.titleLabel?.textColor = .red
                    }
                    cell.textLabel?.font = UIFont.init(name: "Avenir", size: 14)
                    cell.textField?.font = UIFont.init(name: "Avenir", size: 14)
                    cell.tintColor = UIColor(red:0.24, green:0.37, blue:0.91, alpha:1.0)
            }
            // Personal section
            +++ Section("Personal")
            <<< NameRow() {
                $0.title =  "First name"
                $0.placeholder = "Grace"
                $0.add(rule: RuleRequired())
                $0.validationOptions = .validatesOnChange
                $0.tag = "firstname"
                }.cellUpdate { cell, row in
                    if !row.isValid {
                        cell.titleLabel?.textColor = .red
                    }
                    cell.textLabel?.font = UIFont.init(name: "Avenir", size: 14)
                    cell.textField?.font = UIFont.init(name: "Avenir", size: 14)
                    cell.tintColor = UIColor(red:0.24, green:0.37, blue:0.91, alpha:1.0)
            }
            <<< NameRow() {
                $0.title =  "Last name"
                $0.placeholder = "Kelly"
                $0.add(rule: RuleRequired())
                $0.tag = "lastname"
                $0.validationOptions = .validatesOnChange
                }.cellUpdate { cell, row in
                    if !row.isValid {
                        cell.titleLabel?.textColor = .red
                    }
                    cell.textLabel?.font = UIFont.init(name: "Avenir", size: 14)
                    cell.textField?.font = UIFont.init(name: "Avenir", size: 14)
                    cell.tintColor = UIColor(red:0.24, green:0.37, blue:0.91, alpha:1.0)
            }
            
            <<< PhoneRow() {
                $0.title = "Phone"
                $0.placeholder = "5738888888"
                $0.add(rule: RuleRequired())
                $0.validationOptions = .validatesOnChange
                $0.tag = "phone"
                }.cellUpdate { cell, row in
                    if !row.isValid {
                        cell.titleLabel?.textColor = .red
                    }
                    cell.textLabel?.font = UIFont.init(name: "Avenir", size: 14)
                    cell.textField?.font = UIFont.init(name: "Avenir", size: 14)
                    cell.tintColor = UIColor(red:0.24, green:0.37, blue:0.91, alpha:1.0)
            }
            
            // Submit button
            +++ Section(footer: "By sign up you’re agreed to accept all Term of Use and Privacy Policy we provided.")
            <<< ButtonRow() {
                $0.title = "Submit"
                }.cellUpdate{ cell, row in
                    cell.textLabel?.font = UIFont.init(name: "Avenir", size: 14)
                    cell.tintColor = UIColor(red:0.24, green:0.37, blue:0.91, alpha:1.0)
                }.onCellSelection { cell, row in
                    row.section?.form?.validate()
                    
                    let formvalues = self.form.values()
                    
                    if(formvalues["firstname"]! == nil || formvalues["lastname"]! == nil || formvalues["email"]! == nil || formvalues["password"]! == nil || formvalues["code"]! == nil || formvalues["phone"]! == nil) {
                        Loaf("All field must be filled", state: .error, location: .top, sender: self).show()
                    } else {
                        print(formvalues)
                        
                        // Start loading indicator
                        let alert = UIAlertController(title: nil, message: "Loading...", preferredStyle: .alert)
                        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
                        loadingIndicator.hidesWhenStopped = true
                        loadingIndicator.style = UIActivityIndicatorView.Style.gray
                        loadingIndicator.startAnimating();
                        alert.view.addSubview(loadingIndicator)
                        self.present(alert, animated: true, completion: nil)
                        
                        // APIs request submission for medicine to specific user
                        DispatchQueue.main.async {
                            
                            //                            let latitude: String = String(self.locationManager.location!.coordinate.latitude) ?? "null"
                            //                            let longitude: String = String(self.locationManager.location!.coordinate.longitude) ?? "null"
                            //
                            //                            print(self.locationManager.location!.coordinate.latitude)
                            //                            print(self.locationManager.location!.coordinate.longitude)
                            
                            requestSignup(email: formvalues["email"] as? String ?? "null", pass: formvalues["password"] as? String ?? "null", fname: formvalues["firstname"] as? String ?? "null", lname: formvalues["lastname"] as? String ?? "null", phone: formvalues["phone"] as? String ?? "null", code: formvalues["code"] as? String ?? "null", completionHandler: { (result) in
                                
                                if (result == false){
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                                        // Stop loading indicator
                                        self.dismiss(animated: false, completion: nil)
                                        Loaf("Cannot create an account, please try again.", state: .error, location: .top, sender: self).show()
                                    }
                                } else {
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                                        // Stop loading indicator
                                        self.dismiss(animated: false, completion: nil)
                                        // Navigate to TimelineViewController
                                        self.navigationController?.popToRootViewController(animated: true)
                                    }
                                }
                                
                            })
                        }
                    }
        }
    }
    
}
