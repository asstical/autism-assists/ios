//
//  SigninViewController.swift
//  Assists
//
//  Created by Thunpisit Amnuaikiatloet on 8/26/19.
//  Copyright © 2019 Thunpisit Amnuaikiatloet. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import TransitionButton
import Loaf
//import DropDown

class SigninViewController: UIViewController {

    @IBOutlet weak var onboardImage: UIImageView!
    //    @IBOutlet weak var institutionButton: UIButton!
    @IBOutlet weak var emailTextfield: SkyFloatingLabelTextField!
    @IBOutlet weak var passwordTextfield: SkyFloatingLabelTextField!
    @IBOutlet weak var signinButton: TransitionButton!
    @IBOutlet weak var signupButton: UIButton!
    
    var patients: [Patient]? = []
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        title = ""
        
        style()
        customInteraction()
        setupKeyboardDismissRecognizer()
        liftViewKeyboard()
    }
    
    func style() {
        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        } else {
            // Fallback on earlier versions
        }  
        // + CUSTOM NAVIGATION CONTROLLER
        // Change navigation controller button to grey
        self.navigationController?.navigationBar.tintColor = UIColor(red:0.02, green:0.17, blue:0.36, alpha:1.0)
        // Change navigation controller bar to white
        self.navigationController?.navigationBar.barTintColor = UIColor.white
        // Change navigationContrller font to Avenir
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: "Avenir-Medium", size: 16)!]
        // Hide navigationController hairline
        self.navigationController?.navigationBar.setValue(true, forKey: "hidesShadow")
        self.navigationController?.navigationBar.isTranslucent = false
        
        // + CUSTOM VIEW CONTROLLER
        // Change background color
        self.view.backgroundColor = UIColor.white
    }
    
    func customInteraction() {
        // + CUSTOM DROP DOWN
//        let institutionDropDown = DropDown()
//        institutionDropDown.anchorView = institutionButton
//        institutionDropDown.dataSource = ["Car", "Motorcycle", "Truck"]
        
        // + CUSTOM TEXTFIELD
        emailTextfield.selectedTitleColor = UIColor.gray
        emailTextfield.addTarget(self, action: #selector(emailDidChange(_:)), for: .editingChanged)
        emailTextfield.placeholderColor = UIColor(red:0.24, green:0.37, blue:0.91, alpha:1.0)
        emailTextfield.selectedTitleColor = UIColor(red:0.24, green:0.37, blue:0.91, alpha:1.0)
        emailTextfield.selectedLineColor = UIColor(red:0.24, green:0.37, blue:0.91, alpha:1.0)
        
        passwordTextfield.selectedTitleColor = UIColor.gray
        passwordTextfield.addTarget(self, action: #selector(passwordDidChange(_:)), for: .editingChanged)
        passwordTextfield.placeholderColor = UIColor(red:0.24, green:0.37, blue:0.91, alpha:1.0)
        passwordTextfield.selectedTitleColor = UIColor(red:0.24, green:0.37, blue:0.91, alpha:1.0)
        passwordTextfield.selectedLineColor = UIColor(red:0.24, green:0.37, blue:0.91, alpha:1.0)
        
        // + CUSTOM BUTTON
        signinButton.backgroundColor = UIColor(red:0.24, green:0.37, blue:0.91, alpha:1.0)
        signinButton.layer.cornerRadius = 5
        signinButton.layer.borderWidth = 1
        signinButton.layer.borderColor = UIColor(red:0.24, green:0.37, blue:0.91, alpha:1.0).cgColor
        signinButton.setTitleColor(UIColor.white, for: .normal)
        signinButton.setTitle("Login", for: .normal)
//        signinButton.spinnerColor = .white
//        signinButton.addTarget(self, action: #selector(buttonAction(_:)), for: .touchUpInside)
        
        signupButton.setTitleColor(UIColor(red:0.02, green:0.17, blue:0.36, alpha:1.0), for: .normal)
        signupButton.setTitle("Don't have an account? Sign up", for: .normal)
    }
    
    @IBAction func loginToggle(_ button: TransitionButton) {
        button.startAnimation()
        let qualityOfServiceClass = DispatchQoS.QoSClass.background
        let backgroundQueue = DispatchQueue.global(qos: qualityOfServiceClass)
        
        if(self.emailTextfield.text == "" || self.passwordTextfield.text == ""){
            button.stopAnimation(animationStyle: .shake, revertAfterDelay: 1.0, completion: {
                Loaf("Email and password cannot be empty", state: .error, location: .top, sender: self).show()
                self.customInteraction()
            })
        }
        
        backgroundQueue.async(execute: {
            
            DispatchQueue.main.async(execute: { () -> Void in
                
                // APIs authenticate user
                requestSignin(email: self.emailTextfield.text ?? "", pass: self.passwordTextfield.text ?? "", completionHandler: { (key, error) in
                    
                    print("=> Key: " + key)
                    
                    if (key == "null") {
                        button.stopAnimation(animationStyle: .shake, revertAfterDelay: 1.0, completion: {
                            Loaf("Your email or password is invalid, please try again.", state: .error, location: .top, sender: self).show()
                            self.style()
                        })
                    } else if (key == "error") {
                        button.stopAnimation(animationStyle: .shake, revertAfterDelay: 1.0, completion: {
                            Loaf(error, state: .error, location: .top, sender: self).show()
                            self.style()
                        })
                    } else {
                        // APIs request user information
                        requestUser(key: key, completionHandler: { (user) in
                            
                            // Saves user information to UserDefaults
                            UserDefaults.standard.set(user.id, forKey: "user_id")
                            UserDefaults.standard.set(user.firstname, forKey: "user_fname")
                            UserDefaults.standard.set(user.lastname, forKey: "user_lname")
                            UserDefaults.standard.set(user.email, forKey: "user_email")
                            UserDefaults.standard.set(user.role, forKey: "user_role")
                            UserDefaults.standard.set(key, forKey: "user_key")
                            
                            // IF patient is not empty go to PatientsViewController and pass patients data
                            // ELSE go to PatientsEmptyViewController
                            if (user.patients.isEmpty == true) {
                                button.stopAnimation(animationStyle: .shake, revertAfterDelay: 1.0, completion: {
                                    Loaf("No patients associated with this user", state: .warning, location: .top, sender: self).show()
                                    self.customInteraction()
                                    self.style()
                                })
                            } else {
                                
                                // Go to PatientsViewController with patients data
                                DispatchQueue.main.async(execute: { () -> Void in
                                    
                                    // For loop retrieving each patients information
                                    for patient in user.patients {
                                        requestPatient(key: key, pid: patient, completionHandler: { (child) in
                                            self.patients?.append(child)
                                        })
                                    }
                                    
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                                        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                        let patientListViewController = storyBoard.instantiateViewController(withIdentifier: "patientListViewController") as! PatientListViewController
                                        // Send patients information to PatientsViewController
                                        patientListViewController.patients = self.patients
                                        button.stopAnimation(animationStyle: .expand, revertAfterDelay: 1.0, completion: {
                                            patientListViewController.modalPresentationStyle = .fullScreen
                                            self.present(patientListViewController, animated: true, completion: nil)
                                        })
                                    }
                                })
                            }
                        })
                        
                    }
                })
            })
        })
    }
    
    // Email validation
    @objc func emailDidChange(_ textfield: UITextField) {
        if let text = textfield.text {
            if let floatingLabelTextField = textfield as? SkyFloatingLabelTextField {
                if(text.count < 3 || !text.contains("@")) {
                    floatingLabelTextField.errorMessage = "Invalid email"
                }
                else {
                    // The error message will only disappear when we reset it to nil or empty string
                    floatingLabelTextField.errorMessage = ""
                }
            }
        }
    }
    
    // Password validation
    @objc func passwordDidChange(_ textfield: UITextField) {
        if let text = textfield.text {
            if let floatingLabelTextField = textfield as? SkyFloatingLabelTextField {
                if(text.count < 3) {
                    floatingLabelTextField.errorMessage = "Invalid password"
                }
                else {
                    // The error message will only disappear when we reset it to nil or empty string
                    floatingLabelTextField.errorMessage = ""
                }
            }
        }
    }
    
    // Lift view when keyboard is active, and default when inactive
    func liftViewKeyboard(){
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }

    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }

    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    
    // Add tap gesture and when tap view it will dismissed keyboard (https://www.codevscolor.com/ios-swift-dismiss-keyboard-tap-outside/)
    func setupKeyboardDismissRecognizer(){
        let tapRecognizer: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(SigninViewController.dismissKeyboard))
        
        self.view.addGestureRecognizer(tapRecognizer)
    }

    @objc func dismissKeyboard()
    {
        view.endEditing(true)
    }

}
