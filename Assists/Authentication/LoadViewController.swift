//
//  LoadViewController.swift
//  Assists
//
//  Created by Thunpisit Amnuaikiatloet on 9/19/19.
//  Copyright © 2019 Thunpisit Amnuaikiatloet. All rights reserved.
//

import UIKit
import Loaf
import UserNotifications

class LoadViewController: UIViewController, UNUserNotificationCenterDelegate {

    var patients: [Patient]? = []
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
        
        override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(true)
            self.navigationController?.setNavigationBarHidden(true, animated: false)
            //        inSession()
        }
        
        override func viewWillDisappear(_ animated: Bool) {
            super.viewWillDisappear(true)
            self.navigationController?.setNavigationBarHidden(false, animated: false)
        }

        override func viewDidLoad() {
            super.viewDidLoad()
            
            permissionNotification()
//            morningNotification()
//            eveningNotificaton()
            setLocalNotification()
            
            if #available(iOS 13.0, *) {
                overrideUserInterfaceStyle = .light
            } else {
                // Fallback on earlier versions
            }
            activityIndicator.hidesWhenStopped = true
            activityIndicator.style = UIActivityIndicatorView.Style.gray
            activityIndicator.startAnimating()
            inSession()
        }
    
        func permissionNotification() {
            let center = UNUserNotificationCenter.current()

            center.requestAuthorization(options: [.alert, .badge, .sound]) { (granted, error) in
                if granted {
                    print("Notification - Allow")
                } else {
                    print("Notification - Disallow")
                }
            }
        }
    
        func setLocalNotification(){
            
            let center = UNUserNotificationCenter.current()
            center.delegate = self
            
            let uuidMorning = UUID().uuidString
            let uuidEvening = UUID().uuidString

            let show = UNNotificationAction(identifier: "show", title: "Tell me more…", options: .foreground)
            
            let morning = UNNotificationCategory(identifier: uuidMorning, actions: [show], intentIdentifiers: [])
            let evening = UNNotificationCategory(identifier: uuidEvening, actions: [show], intentIdentifiers: [])

            center.setNotificationCategories([morning,evening])
//            center.setNotificationCategories([morning,evening])

            // Morning notification
            let contentMorning = UNMutableNotificationContent()
            contentMorning.title = "How did your child sleep last night?"
            contentMorning.body = "A reminder to record last night sleep in Autism Assists to see sleeping trends."
            contentMorning.userInfo = ["timeOfDay": "morning"]
            contentMorning.sound = UNNotificationSound.default

            var timeMorning = DateComponents()
            timeMorning.hour = 09
            timeMorning.minute = 00

            let triggerMorning = UNCalendarNotificationTrigger(
            dateMatching: timeMorning, repeats: true)

            let requestMorning = UNNotificationRequest(identifier: uuidMorning,
            content: contentMorning, trigger: triggerMorning)

            center.add(requestMorning) { (error) in
                print("Notification errors")
            }

            // Evening notification
            let contentEvening = UNMutableNotificationContent()
            contentEvening.title = "What did your child do today?"
            contentEvening.body = "A reminder to record today activities in Autism Assists"
            contentEvening.userInfo = ["timeOfDay": "morning"]
            contentEvening.sound = UNNotificationSound.default

            var timeEvening = DateComponents()
            timeEvening.hour = 21
            timeEvening.minute = 00

            let triggerEvening = UNCalendarNotificationTrigger(
            dateMatching: timeEvening, repeats: true)

            let requestEvening = UNNotificationRequest(identifier: uuidEvening,
            content: contentEvening, trigger: triggerEvening)

            center.add(requestEvening) { (error) in
                print("Notification errors")
            }

            print(UNUserNotificationCenter.current())

        }

//        func morningNotification() {
//
//            let NotificationUUID = UUID().uuidString
//
//            registerCategories(udid: NotificationUUID)
//
//            let center = UNUserNotificationCenter.current()
//
//            // not required, but useful for testing!
////            center.removeAllPendingNotificationRequests()
//
//            let content = UNMutableNotificationContent()
//            content.title = "How did your child sleep last night?"
//            content.body = "A reminder to record last night sleep in Autism Assists to see sleeping trends."
//            content.categoryIdentifier = "alarm"
//            content.userInfo = ["timeOfDay": "morning"]
//            content.sound = UNNotificationSound.default
//
//            var dateComponents = DateComponents()
//            dateComponents.hour = 23
//            dateComponents.minute = 16
//            let trigger = UNCalendarNotificationTrigger(dateMatching: dateComponents, repeats: true)
//
//            let request = UNNotificationRequest(identifier: NotificationUUID, content: content, trigger: trigger)
//
//            print("=> morningNotification")
//            print(request)
//
//            center.add(request)
//        }
//
//        func eveningNotificaton() {
//
//            let NotificationUUID = UUID().uuidString
//
//            registerCategories(udid: NotificationUUID)
//
//            let center = UNUserNotificationCenter.current()
//
//            // not required, but useful for testing!
////            center.removeAllPendingNotificationRequests()
//
//            let content = UNMutableNotificationContent()
//            content.title = "What did your child do today?"
//            content.body = "A reminder to record today activities in Autism Assists"
//            content.categoryIdentifier = "alarm"
//            content.userInfo = ["timeOfDay": "evening"]
//            content.sound = UNNotificationSound.default
//
//            var dateComponents = DateComponents()
//            dateComponents.hour = 23
//            dateComponents.minute = 15
//            let trigger = UNCalendarNotificationTrigger(dateMatching: dateComponents, repeats: true)
//
//            let request = UNNotificationRequest(identifier: NotificationUUID, content: content, trigger: trigger)
//
//            print("=> eveningNotification")
//            print(request)
//
//            center.add(request)
//        }

//        func registerCategories(udid: String) {
//            let center = UNUserNotificationCenter.current()
//            center.delegate = self
//
//            let show = UNNotificationAction(identifier: "show", title: "Tell me more…", options: .foreground)
//            let category = UNNotificationCategory(identifier: udid, actions: [show], intentIdentifiers: [])
//
//            center.setNotificationCategories([category])
////            center.getNotificationCategories(completionHandler: true)
//
//            print("===> NOTIFICATIONS")
//            print()
//        }

        func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
            // pull out the buried userInfo dictionary
            let userInfo = response.notification.request.content.userInfo

            if let customData = userInfo["timeOfDay"] as? String {
                print("Custom data received: \(customData)")

                switch response.actionIdentifier {
                case UNNotificationDefaultActionIdentifier:
                    // the user swiped to unlock; do nothing
                    print("Default identifier")

                case "show":
                    print("Show more information…")

                default:
                    break
                }
            }

            // you need to call the completion handler when you're done
            completionHandler()
        }

        // Check if there is still user_key if yes check for patient_id
        func inSession(){
            
    //        let uid = UserDefaults.standard.string(forKey: "user_id") ?? "null"
            let key = UserDefaults.standard.string(forKey: "user_key") ?? "null"
            let pid = UserDefaults.standard.string(forKey: "patient_id") ?? "null"
            
            let qualityOfServiceClass = DispatchQoS.QoSClass.background
            let backgroundQueue = DispatchQueue.global(qos: qualityOfServiceClass)
            
            if(key != "null"){
                
                // Request patient with user key and redirect to patients list
                backgroundQueue.async(execute: {
                    
                    DispatchQueue.main.async(execute: { () -> Void in
                        
                        requestUser(key: key, completionHandler: { (user) in
                            
                            // Saves user information to UserDefaults
                            UserDefaults.standard.set(user.id, forKey: "user_id")
                            UserDefaults.standard.set(user.firstname, forKey: "user_fname")
                            UserDefaults.standard.set(user.lastname, forKey: "user_lname")
                            UserDefaults.standard.set(user.email, forKey: "user_email")
                            UserDefaults.standard.set(user.role, forKey: "user_role")
                            UserDefaults.standard.set(key, forKey: "user_key")
                            
                            // IF patient is not empty go to PatientsViewController and pass patients data
                            // ELSE go to PatientsEmptyViewController
                            if (user.patients.isEmpty == true) {
                                // Go to PatientsEmptyViewController
                                
                                Loaf("No patient found, redirect to Signin.", state: .error, location: .top, sender: self).show()
                                
                                DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) { // Change `2.0`
                                    self.activityIndicator.stopAnimating()
                                    let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                    let signinViewController = storyBoard.instantiateViewController(withIdentifier: "signinViewController") as! SigninViewController
//                                    self.present(signinViewController, animated: true, completion: nil)
                                    self.navigationController?.pushViewController(signinViewController, animated: true)
                                }
                            } else {
                                
                                // Go to PatientsViewController with patients data
                                DispatchQueue.main.async(execute: { () -> Void in
                                    
                                    // For loop retrieving each patients information
                                    for patient in user.patients {
                                        requestPatient(key: key, pid: patient, completionHandler: { (child) in
                                            self.patients?.append(child)
                                        })
                                    }
                                    
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                                        self.activityIndicator.stopAnimating()
                                        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                        let patientListViewController = storyBoard.instantiateViewController(withIdentifier: "patientListViewController") as! PatientListViewController
                                        patientListViewController.patients = self.patients
                                       
                                       patientListViewController.modalPresentationStyle = .fullScreen
                                        self.present(patientListViewController, animated: true, completion: nil)
//                                        self.navigationController?.pushViewController(patientListViewController, animated: true)
                                    }
                                })
                            }
                        })
                        
                    })
                })
                
            } else {
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                    print("user key is empty, redirect to sign in")
                    self.activityIndicator.stopAnimating()
                    let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
    //                let nextViewController = storyBoard.instantiateViewController(withIdentifier: "signinNavigationController") as! UINavigationController
                    let nextViewController = storyBoard.instantiateViewController(withIdentifier: "signinViewController") as! SigninViewController
                    self.navigationController?.pushViewController(nextViewController, animated: true)
                }
            }
            
        }

    }
