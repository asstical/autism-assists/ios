//
//  PatientListViewController.swift
//  Assists
//
//  Created by Thunpisit Amnuaikiatloet on 8/27/19.
//  Copyright © 2019 Thunpisit Amnuaikiatloet. All rights reserved.
//

import UIKit

class PatientListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableview: UITableView!
    
    var patients: [Patient]? = []

    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Patients"
        style()
    }
    
    func style() {
        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        } else {
            // Fallback on earlier versions
        }
        // + CUSTOM NAVIGATION CONTROLLER
        // Change navigation controller button to grey
        self.navigationController?.navigationBar.tintColor = UIColor(red:0.02, green:0.17, blue:0.36, alpha:1.0)
        // Change navigation controller bar to white
        self.navigationController?.navigationBar.barTintColor = UIColor.white
        // Change navigationContrller font to Avenir
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: "Avenir-Medium", size: 16)!]
        // Hide navigationController hairline
//        self.navigationController?.navigationBar.setValue(true, forKey: "hidesShadow")
//        self.navigationController?.navigationBar.isTranslucent = false
        
        // + CUSTOM VIEW CONTROLLER
        // Change background color
        self.view.backgroundColor = UIColor.white
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return patients?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "patientCell") as! PatientListTableViewCell
        let patient = patients?[indexPath.row]
        
        cell.patientName.text = patient!.firstname.capitalized + " " + patient!.lastname.capitalized + " (" + patient!.nickname.capitalized + ")"
        cell.patientId.text = "ID: " + patient!.institutionId.uppercased()
        cell.patientInstitution.text = "In care of Dr." + patient!.doctor.capitalized + ", " + patient!.institution.capitalized
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableview.deselectRow(at: indexPath, animated: true)
        
        let patient = patients![indexPath.row]
        UserDefaults.standard.set(patient.id, forKey: "patient_id")
        UserDefaults.standard.set(patient.firstname, forKey: "patient_fname")
        UserDefaults.standard.set(patient.lastname, forKey: "patient_lname")
        UserDefaults.standard.set(patient.nickname, forKey: "patient_nname")
        UserDefaults.standard.set(patient.institutionId, forKey: "patient_insid")
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "menuBarController") as! UITabBarController
        nextViewController.modalPresentationStyle = .fullScreen
        self.present(nextViewController, animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let indexPath = tableview.indexPathForSelectedRow,
            let numPatients = patients?.count,
            indexPath.row < numPatients,
            let patient = patients?[indexPath.row] {
            //            print(patient.fname)
            UserDefaults.standard.set(patient.id, forKey: "patient_id")
            UserDefaults.standard.set(patient.firstname, forKey: "patient_fname")
            UserDefaults.standard.set(patient.lastname, forKey: "patient_lname")
            UserDefaults.standard.set(patient.nickname, forKey: "patient_nname")
            UserDefaults.standard.set(patient.institutionId, forKey: "patient_insid")
        }
    }

}
