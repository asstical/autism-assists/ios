//
//  PatientListTableViewCell.swift
//  Assists
//
//  Created by Thunpisit Amnuaikiatloet on 9/10/19.
//  Copyright © 2019 Thunpisit Amnuaikiatloet. All rights reserved.
//

import UIKit

class PatientListTableViewCell: UITableViewCell {

    @IBOutlet weak var patientImage: UIImageView!
    @IBOutlet weak var patientName: UILabel!
    @IBOutlet weak var patientId: UILabel!
    @IBOutlet weak var patientInstitution: UILabel!
    @IBOutlet weak var cardView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
//        cardView.layer.cornerRadius = 10.0
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func prepareForReuse() {
        patientName.text = nil
        patientId.text = nil
        patientInstitution.text = nil
    }

}
