//
//  AddActivityViewController.swift
//  Assists
//
//  Created by Thunpisit Amnuaikiatloet on 9/4/19.
//  Copyright © 2019 Thunpisit Amnuaikiatloet. All rights reserved.
//

import UIKit
import CoreLocation
import Loaf
import Eureka

class AddActivityViewController: FormViewController {

    var key = UserDefaults.standard.string(forKey: "user_key") ?? "null"
    var pid = UserDefaults.standard.string(forKey: "patient_id") ?? "null"
    
    var type: String = ""
    var subtype: String = ""
    var one: String = ""
    var two: String = ""
    var three: String = ""
    var four: String = ""
    var five: String = ""
    
    var latitude: String = "null"
    var longitude: String = "null"
    
    let locationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Add " + type
        
        setupForm()
        locationManager.requestAlwaysAuthorization()
        print(type)
        print(subtype)
    }
    
    // Form using Eureka framework with validation
    func setupForm(){
        
        LabelRow.defaultCellUpdate = { cell, row in
            cell.contentView.backgroundColor = .red
            cell.textLabel?.textColor = .white
            cell.textLabel?.font = UIFont.init(name: "Avenir", size: 14)
            cell.textLabel?.textAlignment = .right
            
        }
        
        TextRow.defaultCellUpdate = { cell, row in
            if !row.isValid {
                cell.titleLabel?.textColor = .red
            }
        }
        
        form
            // Credential section
            +++ Section(String(subtype.capitalized))
            
            <<< PickerInputRow<String>(""){
                $0.title = "Level"
                $0.tag = "level"
                $0.options = [one, two, three, four, five]
                $0.add(rule: RuleRequired())
                $0.validationOptions = .validatesOnChange
                $0.value = $0.options.first
                }.cellUpdate { cell, row in
                    if !row.isValid {
                        //                        cell.titleLabel?.textColor = .red
                        cell.detailTextLabel?.textColor = UIColor.red
                    }
                    cell.textLabel?.font = UIFont.init(name: "Avenir", size: 14)
                    //                    cell.textField?.font = UIFont.init(name: "Avenir", size: 14)
                    cell.detailTextLabel?.font = UIFont.init(name: "Avenir", size: 14)
                    cell.detailTextLabel?.font = UIFont.init(name: "Avenir", size: 14)
                    cell.tintColor = UIColor(red:0.24, green:0.37, blue:0.91, alpha:1.0)
            }
            
            <<< TextAreaRow("notes") {
                $0.placeholder = "Notes"
                $0.tag = "notes"
                $0.textAreaHeight = .dynamic(initialTextViewHeight: 100)
                }.cellUpdate { cell, row in
                    cell.textLabel?.font = UIFont.init(name: "Avenir", size: 14)
                    //                    cell.textField?.font = UIFont.init(name: "Avenir", size: 14)
                    cell.detailTextLabel?.font = UIFont.init(name: "Avenir", size: 14)
                    cell.detailTextLabel?.font = UIFont.init(name: "Avenir", size: 14)
                    cell.tintColor = UIColor(red:0.24, green:0.37, blue:0.91, alpha:1.0)
                    cell.placeholderLabel?.font = UIFont.init(name: "Avenir", size: 14)
                    cell.textView?.font = UIFont.init(name: "Avenir", size: 14)
            }
            
            // Submit button
            //            +++ Section()
            <<< ButtonRow() {
                $0.title = "Submit"
                }.cellUpdate{ cell, row in
                    cell.textLabel?.font = UIFont.init(name: "Avenir", size: 14)
                    cell.tintColor = UIColor(red:0.24, green:0.37, blue:0.91, alpha:1.0)
                }.onCellSelection{ cell, row in
                    
                    row.section?.form?.validate()
                    let formvalues = self.form.values()
                    
                    if(formvalues["level"]! == nil ) {
                        Loaf("Level field can't be empty", state: .error, location: .top, sender: self).show()
                    } else {
                        print(formvalues)
                        
                        // Start loading indicator
                        let alert = UIAlertController(title: nil, message: "Loading...", preferredStyle: .alert)
                        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
                        loadingIndicator.hidesWhenStopped = true
                        loadingIndicator.style = UIActivityIndicatorView.Style.gray
                        loadingIndicator.startAnimating();
                        alert.view.addSubview(loadingIndicator)
                        self.present(alert, animated: true, completion: nil)
                        
                        //                        formvalues["time"] as? String ?? "null"
                        
                        // APIs request submission for medicine to specific user
                        DispatchQueue.main.async {
                            
                            // Check wheather user allow location or not.
                            if (self.locationManager.location == nil){
                                self.latitude = "null"
                                self.longitude = "null"
                            } else {
                                self.latitude = String(self.locationManager.location!.coordinate.latitude)
                                self.longitude = String(self.locationManager.location!.coordinate.longitude)
                            }
                            
                            requestAddActivity(pid: self.pid, key: self.key, type: self.type, subtype: self.subtype, rating: self.identifyRating(level: formvalues["level"] as? String ?? "null"), rating_des: formvalues["level"] as? String ?? "null", raw_notes: formvalues["notes"] as? String ?? "null", lat: self.latitude, long: self.longitude, completionHandler: { (result) in
                                
                                if (result == false) {
                                    // Stop loading indicator
                                    self.dismiss(animated: false, completion: nil)
                                    Loaf("Something went wrong, we can't add activity record. Please try again.", state: .error, location: .top, sender: self).show()
                                } else if (result == true) {
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                                        // Stop loading indicator
                                        self.dismiss(animated: false, completion: nil)
                                        // Navigate to TimelineViewController
                                        self.navigationController?.popToRootViewController(animated: true)
                                    }
                                }
                            })
                        }
                        
                    }
        }
    }
    
    func identifyRating(level: String) -> String {
        
        var value: String = "null"
        
        if(level == one){
            value = "1"
        } else if(level == two){
            value = "2"
        } else if(level == three){
            value = "3"
        } else if(level == four){
            value = "4"
        } else if(level == five){
            value = "5"
        } else {
            value = "null"
        }
        
        return value
    }

}
