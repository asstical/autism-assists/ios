//
//  AddSleepViewController.swift
//  Assists
//
//  Created by Thunpisit Amnuaikiatloet on 9/4/19.
//  Copyright © 2019 Thunpisit Amnuaikiatloet. All rights reserved.
//

import UIKit
import CoreLocation
import Eureka
import Loaf

class AddSleepViewController: FormViewController {

    var key = UserDefaults.standard.string(forKey: "user_key") ?? "null"
    var pid = UserDefaults.standard.string(forKey: "patient_id") ?? "null"
    
    let locationManager = CLLocationManager()
    
    var latitude: String = "null"
    var longitude: String = "null"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Add sleep"
        self.navigationController!.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: "Avenir-Medium", size: 17)!]
        locationManager.requestWhenInUseAuthorization()
        setupForm()
    }
    
    // Form using Eureka framework with validation
    func setupForm(){
        
        LabelRow.defaultCellUpdate = { cell, row in
            cell.contentView.backgroundColor = .red
            cell.textLabel?.textColor = .white
            cell.textLabel?.font = UIFont.init(name: "Avenir", size: 14)
            cell.textLabel?.textAlignment = .right
            
        }
        
        TextRow.defaultCellUpdate = { cell, row in
            if !row.isValid {
                cell.titleLabel?.textColor = .red
            }
        }
        
        form
            // Credential section
            +++ Section("Sleep")
            
            <<< StepperRow() {
                $0.title = "Duration (hours)"
                $0.tag = "duration"
                $0.value = 1
                $0.add(rule: RuleRequired())
                $0.validationOptions = .validatesOnChange
                }.cellUpdate { cell, row in
                    if !row.isValid {
                        //                        cell.titleLabel?.textColor = .red
                        cell.detailTextLabel?.textColor = UIColor.red
                    }
                    cell.textLabel?.font = UIFont.init(name: "Avenir", size: 14)
                    cell.valueLabel?.text = "\(Int(row.value!))"
                    cell.detailTextLabel?.font = UIFont.init(name: "Avenir", size: 14)
                    cell.detailTextLabel?.font = UIFont.init(name: "Avenir", size: 14)
                    cell.tintColor = UIColor(red:0.24, green:0.37, blue:0.91, alpha:1.0)
                    cell.valueLabel?.font = UIFont.init(name: "Avenir", size: 14)
            }
            
            <<< PickerInputRow<String>("depth"){
                $0.title = "Depth"
                $0.options = ["Light sleep", "Deep sleep", "REM sleep"]
                $0.value = $0.options.first
                }.cellUpdate { cell, row in
                    if !row.isValid {
                        //                        cell.titleLabel?.textColor = .red
                        cell.detailTextLabel?.textColor = UIColor.red
                    }
                    cell.textLabel?.font = UIFont.init(name: "Avenir", size: 14)
                    //                    cell.textField?.font = UIFont.init(name: "Avenir", size: 14)
                    cell.detailTextLabel?.font = UIFont.init(name: "Avenir", size: 14)
                    cell.detailTextLabel?.font = UIFont.init(name: "Avenir", size: 14)
                    cell.tintColor = UIColor(red:0.24, green:0.37, blue:0.91, alpha:1.0)
            }
            
            <<< PickerInputRow<String>("interuption"){
                $0.title = "Interuption"
                $0.options = ["Yes", "No"]
                $0.value = $0.options.last
                $0.tag = "interuption"
                $0.add(rule: RuleRequired())
                $0.validationOptions = .validatesOnChange
                }.cellUpdate { cell, row in
                    if !row.isValid {
                        //                        cell.titleLabel?.textColor = .red
                        cell.detailTextLabel?.textColor = UIColor.red
                    }
                    cell.textLabel?.font = UIFont.init(name: "Avenir", size: 14)
                    //                    cell.textField?.font = UIFont.init(name: "Avenir", size: 14)
                    cell.detailTextLabel?.font = UIFont.init(name: "Avenir", size: 14)
                    cell.detailTextLabel?.font = UIFont.init(name: "Avenir", size: 14)
                    cell.tintColor = UIColor(red:0.24, green:0.37, blue:0.91, alpha:1.0)
            }
            
            <<< PickerInputRow<String>("snoring"){
                $0.title = "Snoring"
                $0.options = ["Yes", "No"]
                $0.value = $0.options.last
                $0.tag = "snoring"
                $0.add(rule: RuleRequired())
                $0.validationOptions = .validatesOnChange
                }.cellUpdate { cell, row in
                    if !row.isValid {
                        //                        cell.titleLabel?.textColor = .red
                        cell.detailTextLabel?.textColor = UIColor.red
                    }
                    cell.textLabel?.font = UIFont.init(name: "Avenir", size: 14)
                    //                    cell.textField?.font = UIFont.init(name: "Avenir", size: 14)
                    cell.detailTextLabel?.font = UIFont.init(name: "Avenir", size: 14)
                    cell.detailTextLabel?.font = UIFont.init(name: "Avenir", size: 14)
                    cell.tintColor = UIColor(red:0.24, green:0.37, blue:0.91, alpha:1.0)
            }
            
            <<< StepperRow() {
                $0.title = "Time to sleep (mins)"
                $0.tag = "time_to_sleep"
                $0.value = 1
                $0.add(rule: RuleRequired())
                $0.validationOptions = .validatesOnChange
                }.cellUpdate { cell, row in
                    if !row.isValid {
                        //                        cell.titleLabel?.textColor = .red
                        cell.detailTextLabel?.textColor = UIColor.red
                    }
                    cell.textLabel?.font = UIFont.init(name: "Avenir", size: 14)
                    cell.valueLabel?.text = "\(Int(row.value!))"
                    cell.detailTextLabel?.font = UIFont.init(name: "Avenir", size: 14)
                    cell.detailTextLabel?.font = UIFont.init(name: "Avenir", size: 14)
                    cell.tintColor = UIColor(red:0.24, green:0.37, blue:0.91, alpha:1.0)
                    cell.valueLabel?.font = UIFont.init(name: "Avenir", size: 14)
            }
            
            <<< StepperRow() {
                $0.title = "Time to get up (mins)"
                $0.tag = "time_to_up"
                $0.value = 1
                $0.add(rule: RuleRequired())
                $0.validationOptions = .validatesOnChange
                }.cellUpdate { cell, row in
                    if !row.isValid {
                        //                        cell.titleLabel?.textColor = .red
                        cell.detailTextLabel?.textColor = UIColor.red
                    }
                    cell.textLabel?.font = UIFont.init(name: "Avenir", size: 14)
                    cell.valueLabel?.text = "\(Int(row.value!))"
                    cell.detailTextLabel?.font = UIFont.init(name: "Avenir", size: 14)
                    cell.detailTextLabel?.font = UIFont.init(name: "Avenir", size: 14)
                    cell.tintColor = UIColor(red:0.24, green:0.37, blue:0.91, alpha:1.0)
                    cell.valueLabel?.font = UIFont.init(name: "Avenir", size: 14)
            }
            
            <<< TextAreaRow("notes") {
                $0.placeholder = "Notes"
                $0.textAreaHeight = .dynamic(initialTextViewHeight: 100)
                }.cellUpdate { cell, row in
                    cell.textLabel?.font = UIFont.init(name: "Avenir", size: 14)
                    //                    cell.textField?.font = UIFont.init(name: "Avenir", size: 14)
                    cell.detailTextLabel?.font = UIFont.init(name: "Avenir", size: 14)
                    cell.detailTextLabel?.font = UIFont.init(name: "Avenir", size: 14)
                    cell.tintColor = UIColor(red:0.24, green:0.37, blue:0.91, alpha:1.0)
                    cell.placeholderLabel?.font = UIFont.init(name: "Avenir", size: 14)
                    cell.textView?.font = UIFont.init(name: "Avenir", size: 14)
            }
            
            // Submit button
            //            +++ Section()
            <<< ButtonRow() {
                $0.title = "Submit"
                }.cellUpdate{ cell, row in
                    cell.textLabel?.font = UIFont.init(name: "Avenir", size: 14)
                    cell.tintColor = UIColor(red:0.24, green:0.37, blue:0.91, alpha:1.0)
                }.onCellSelection { cell, row in
                    row.section?.form?.validate()
                    
                    let formvalues = self.form.values()
                    //                    print(formvalues["time_to_sleep"]! as! String)
                    
                    print(String(format:"%.1f", formvalues["time_to_sleep"]! as! CVarArg))
                    
                    DispatchQueue.main.async {
                        
                        // Check wheather user allow location or not.
                        if (self.locationManager.location == nil){
                            self.latitude = "null"
                            self.longitude = "null"
                        } else {
                            self.latitude = String(self.locationManager.location!.coordinate.latitude)
                            self.longitude = String(self.locationManager.location!.coordinate.longitude)
                        }
                        
                        // Start loading indicator
                        let alert = UIAlertController(title: nil, message: "Loading...", preferredStyle: .alert)
                        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
                        loadingIndicator.hidesWhenStopped = true
                        loadingIndicator.style = UIActivityIndicatorView.Style.gray
                        loadingIndicator.startAnimating();
                        alert.view.addSubview(loadingIndicator)
                        self.present(alert, animated: true, completion: nil)
                        
                        // APIs request submission for sleep to specific patient
                        requestAddSleep(pid: self.pid, key: self.key, duration: String(format:"%.1f", formvalues["duration"]! as! CVarArg), depth: formvalues["depth"] as? String ?? "null", interuption: formvalues["interuption"] as? String ?? "null", snoring: formvalues["snoring"] as? String ?? "null", time_to_sleep: String(format:"%.1f", formvalues["time_to_sleep"]! as! CVarArg) , time_to_up: String(format:"%.1f", formvalues["time_to_up"]! as! CVarArg) , raw_notes: formvalues["notes"] as? String ?? "null", lat: self.latitude, long: self.longitude, completionHandler: { (result) in
                            
                            if (result == false) {
                                // Stop loading indicator
                                self.dismiss(animated: false, completion: nil)
                                Loaf("Something went wrong, we can't add sleep record. Please try again.", state: .error, location: .top, sender: self).show()
                            } else if (result == true) {
                                DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                                    // Stop loading indicator
                                    self.dismiss(animated: false, completion: nil)
                                    self.navigationController?.popToRootViewController(animated: true)
                                }
                            }
                        })
                    }
        }
    }
}

extension Double {
    
    var stringValue: String {
        
        return String(format: "%.1f", self)
    }
}
