//
//  AddMedicineViewController.swift
//  Assists
//
//  Created by Thunpisit Amnuaikiatloet on 9/4/19.
//  Copyright © 2019 Thunpisit Amnuaikiatloet. All rights reserved.
//

import UIKit
import CoreLocation
import Eureka
import Loaf

class AddMedicineViewController: FormViewController {

    var key = UserDefaults.standard.string(forKey: "user_key") ?? "null"
    var pid = UserDefaults.standard.string(forKey: "patient_id") ?? "null"
    
    let locationManager = CLLocationManager()
    
    var latitude: String = "null"
    var longitude: String = "null"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Add medicine"
        self.navigationController!.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: "Avenir-Medium", size: 17)!]
        setupForm()
        locationManager.requestAlwaysAuthorization()
    }
    
    // Form using Eureka framework with validation
    func setupForm(){
        
        LabelRow.defaultCellUpdate = { cell, row in
            cell.contentView.backgroundColor = .red
            cell.textLabel?.textColor = .white
            cell.textLabel?.font = UIFont.init(name: "Avenir", size: 14)
            cell.textLabel?.textAlignment = .right
            
        }
        
        TextRow.defaultCellUpdate = { cell, row in
            if !row.isValid {
                cell.titleLabel?.textColor = .red
            }
        }
        
        form
            // Credential section
            +++ Section("Medication")
            // Email with validation
            <<< TextRow() {
                $0.title = "Name"
                $0.tag = "name"
                $0.placeholder = "Ibuprofen"
                $0.add(rule: RuleRequired())
                $0.validationOptions = .validatesOnChange
                }.cellUpdate { cell, row in
                    if !row.isValid {
                        cell.titleLabel?.textColor = .red
                    }
                    cell.textLabel?.font = UIFont.init(name: "Avenir", size: 14)
                    cell.textField?.font = UIFont.init(name: "Avenir", size: 14)
                    cell.tintColor = UIColor(red:0.24, green:0.37, blue:0.91, alpha:1.0)
            }
            <<< TextRow() {
                $0.title = "Dosage"
                $0.tag = "dosage"
                $0.placeholder = "100mg"
                $0.add(rule: RuleRequired())
                $0.validationOptions = .validatesOnChange
                }.cellUpdate { cell, row in
                    if !row.isValid {
                        cell.titleLabel?.textColor = .red
                    }
                    cell.textLabel?.font = UIFont.init(name: "Avenir", size: 14)
                    cell.textField?.font = UIFont.init(name: "Avenir", size: 14)
                    cell.tintColor = UIColor(red:0.24, green:0.37, blue:0.91, alpha:1.0)
            }
            
            <<< StepperRow() {
                $0.title = "Quantity"
                $0.tag = "quantity"
                $0.value = 1
                $0.add(rule: RuleRequired())
                $0.validationOptions = .validatesOnChange
                }.cellUpdate { cell, row in
                    if !row.isValid {
                        //                        cell.titleLabel?.textColor = .red
                        cell.detailTextLabel?.textColor = UIColor.red
                    }
                    cell.textLabel?.font = UIFont.init(name: "Avenir", size: 14)
                    cell.valueLabel?.text = "\(Int(row.value!))"
                    cell.detailTextLabel?.font = UIFont.init(name: "Avenir", size: 14)
                    cell.detailTextLabel?.font = UIFont.init(name: "Avenir", size: 14)
                    cell.tintColor = UIColor(red:0.24, green:0.37, blue:0.91, alpha:1.0)
                    cell.valueLabel?.font = UIFont.init(name: "Avenir", size: 14)
            }
            
            <<< PickerInputRow<String>(""){
                $0.title = "Time"
                $0.tag = "time"
                $0.value = "Before brekfast"
                $0.add(rule: RuleRequired())
                $0.validationOptions = .validatesOnChange
                $0.options = ["Before brekfast", "After brekfast", "Before lunch", "After lunch", "Before dinner", "After dinner", "Before bed"]
                //                for i in 1...10{
                //                    $0.options.append("option \(i)")
                //                }
                $0.value = $0.options.first
                }.cellUpdate { cell, row in
                    if !row.isValid {
                        //                        cell.titleLabel?.textColor = .red
                        cell.detailTextLabel?.textColor = UIColor.red
                    }
                    cell.textLabel?.font = UIFont.init(name: "Avenir", size: 14)
                    //                    cell.textField?.font = UIFont.init(name: "Avenir", size: 14)
                    cell.detailTextLabel?.font = UIFont.init(name: "Avenir", size: 14)
                    cell.detailTextLabel?.font = UIFont.init(name: "Avenir", size: 14)
                    cell.tintColor = UIColor(red:0.24, green:0.37, blue:0.91, alpha:1.0)
            }
            
            <<< TextAreaRow("notes") {
                $0.placeholder = "Notes"
                $0.tag = "notes"
                $0.textAreaHeight = .dynamic(initialTextViewHeight: 100)
                }.cellUpdate { cell, row in
                    cell.textLabel?.font = UIFont.init(name: "Avenir", size: 14)
                    //                    cell.textField?.font = UIFont.init(name: "Avenir", size: 14)
                    cell.detailTextLabel?.font = UIFont.init(name: "Avenir", size: 14)
                    cell.detailTextLabel?.font = UIFont.init(name: "Avenir", size: 14)
                    cell.tintColor = UIColor(red:0.24, green:0.37, blue:0.91, alpha:1.0)
                    cell.placeholderLabel?.font = UIFont.init(name: "Avenir", size: 14)
                    cell.textView?.font = UIFont.init(name: "Avenir", size: 14)
            }
            
            // Submit button
            //            +++ Section()
            <<< ButtonRow() {
                $0.title = "Submit"
                }.cellUpdate{ cell, row in
                    cell.textLabel?.font = UIFont.init(name: "Avenir", size: 14)
                    cell.tintColor = UIColor(red:0.24, green:0.37, blue:0.91, alpha:1.0)
                }.onCellSelection { cell, row in
                    row.section?.form?.validate()
                    
                    let formvalues = self.form.values()
                    
                    if(formvalues["dosage"]! == nil || formvalues["name"]! == nil) {
                        Loaf("Medicine name & dosage can't be empty", state: .error, location: .top, sender: self).show()
                    } else {
                        print(formvalues)
                        
                        // Start loading indicator
                        let alert = UIAlertController(title: nil, message: "Loading...", preferredStyle: .alert)
                        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
                        loadingIndicator.hidesWhenStopped = true
                        loadingIndicator.style = UIActivityIndicatorView.Style.gray
                        loadingIndicator.startAnimating();
                        alert.view.addSubview(loadingIndicator)
                        self.present(alert, animated: true, completion: nil)
                        
                        // APIs request submission for medicine to specific user
                        DispatchQueue.main.async {
                            
                            // Check wheather user allow location or not.
                            if (self.locationManager.location == nil){
                                self.latitude = "null"
                                self.longitude = "null"
                            } else {
                                self.latitude = String(self.locationManager.location!.coordinate.latitude)
                                self.longitude = String(self.locationManager.location!.coordinate.longitude)
                            }
                            
                            requestAddMedicine(pid: self.pid, key: self.key, name: formvalues["name"] as? String ?? "null", dosage: formvalues["dosage"] as? String ?? "null", quantity: String(format:"%.1f", formvalues["quantity"]! as! CVarArg), time_of_day: formvalues["time"] as? String ?? "null", raw_notes: formvalues["notes"] as? String ?? "null", lat: self.latitude, long: self.longitude, completionHandler: { (result) in

                                if (result == false) {
                                    // Stop loading indicator
                                    self.dismiss(animated: false, completion: nil)
                                    Loaf("Something went wrong, we can't add medicine record. Please try again.", state: .error, location: .top, sender: self).show()
                                } else if (result == true) {
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                                        // Stop loading indicator
                                        self.dismiss(animated: false, completion: nil)
                                        // Navigate to TimelineViewController
                                        self.navigationController?.popToRootViewController(animated: true)
                                    }
                                }
                            })
                        }
                    }
        }
    }
    
}
