//
//  AddViewController.swift
//  Assists
//
//  Created by Thunpisit Amnuaikiatloet on 8/27/19.
//  Copyright © 2019 Thunpisit Amnuaikiatloet. All rights reserved.
//

import UIKit
import CollectionViewShelfLayout

class AddViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    

    let medicalItems = [["subtype": "stomachache","one": "😥","two": "😨","three": "😰","four": "😱","five": "🥵", "description": "Pain from inside the abdomen or the outer muscle wall"],
                        ["subtype": "headache","one": "😥","two": "😨","three": "😰","four": "😱","five": "🥵", "description": "A painful sensation in any part of the head, ranging from sharp to dull"],
                        ["subtype": "seizures","one": "😥","two": "😨","three": "😰","four": "😱","five": "🥵", "description": "A disorder in which nerve cell activity in the brain is disturbed"],
                        ["subtype": "diarrhea","one": "😥","two": "😨","three": "😰","four": "😱","five": "🥵", "description": "Loose, watery bowel movements that may occur frequently and with a sense of urgency."],
                        ["subtype": "nutrition_concerns","one": "😥","two": "😨","three": "😰","four": "😱","five": "🥵", "description": "Altered energy and nutrient needs. Mutrient interactions. Unusual food habits"]]
    
    let behaviorItems = [["subtype": "hyperactivity","one": "😐","two": "🙁","three": "😫","four": "😤","five": "🤯", "description": "state of being unusually or abnormally active."],
                         ["subtype": "repeated_thoughts","one": "😐","two": "🙁","three": "😫","four": "😤","five": "🤯", "description": "The process of continuously thinking about the same thoughts"],
                         ["subtype": "impulsivity","one": "😐","two": "🙁","three": "😫","four": "😤","five": "🤯", "description": "A tendency to act on a whim, displaying behavior characterized by little or no forethought, reflection, or consideration of the consequences."],
                         ["subtype": "short_attention","one": "😐","two": "🙁","three": "😫","four": "😤","five": "🤯", "description": "Trouble focusing on tasks for any length of time without being easily distracted."],
                         ["subtype": "self_harm","one": "😐","two": "🙁","three": "😫","four": "😤","five": "🤯", "description": "The act of deliberately harming your own body"],
                         ["subtype": "defiant","one": "😐","two": "🙁","three": "😫","four": "😤","five": "🤯", "description": "Full of or showing a disposition to challenge, resist, or fight"],
                         ["subtype": "tantrum","one": "😐","two": "🙁","three": "😫","four": "😤","five": "🤯", "description": "An uncontrolled outburst of anger and frustration"]]
    
    let emotionItems = [["subtype": "angry","one": "😐","two": "🙁","three": "😫","four": "😤","five": "🤯", "description": ""],
                        ["subtype": "anxious","one": "😐","two": "🙁","three": "😫","four": "😤","five": "🤯", "description": ""],
                        ["subtype": "calm","one": "🙂","two": "😌","three": "😊","four": "😚","five": "🥰", "description": ""],
                        ["subtype": "excited","one": "🙂","two": "😌","three": "😊","four": "😚","five": "🥰", "description": ""],
                        ["subtype": "happy","one": "🙂","two": "😌","three": "😊","four": "😚","five": "🥰", "description": ""],
                        ["subtype": "sad","one": "😐","two": "🙁","three": "😫","four": "😤","five": "🤯", "description": ""],
                        ["subtype": "irritable","one": "😐","two": "🙁","three": "😫","four": "😤","five": "🤯", "description": ""]]
    
    let sleepItems = [["subtype":"sleep", "description": "Record sleeping behavior like duration, depth, snoring, and interuption."]]
    let medicineItems = [["subtype":"medicine", "description": "Record your daily or one-time taken medicine."]]
    
    var categorySections = [AddCategory]()
    
    @IBOutlet weak var tableview: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        style()
        setupCategory()
        self.navigationItem.title = "Add record"
    }
    
    func style() {
        // + CUSTOM NAVIGATION CONTROLLER
        // Change navigation controller button to grey
        self.navigationController?.navigationBar.tintColor = UIColor(red:0.02, green:0.17, blue:0.36, alpha:1.0)
        // Change navigation controller bar to white
        self.navigationController?.navigationBar.barTintColor = UIColor.white
        // Change navigationContrller font to Avenir
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: "Avenir-Medium", size: 16)!]
        // Hide navigationController hairline
        self.navigationController?.navigationBar.setValue(true, forKey: "hidesShadow")
        self.navigationController?.navigationBar.isTranslucent = false
        
        // + CUSTOM VIEW CONTROLLER
        // Change background color
        self.view.backgroundColor = UIColor(red:0.93, green:0.93, blue:0.93, alpha:1.0)
    }
    
    func setupCategory() {
        categorySections = [AddCategory(name: "sleep", items: sleepItems), AddCategory(name: "medical", items: medicalItems), AddCategory(name: "behavior", items: behaviorItems), AddCategory(name: "emotion", items: emotionItems),  AddCategory(name: "medicine", items: medicineItems)]
        print(categorySections)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.categorySections.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return self.categorySections[section].name.capitalized
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let items = self.categorySections[section].items
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "addCell") as! AddTableViewCell
        let items = self.categorySections[indexPath.section].items
        let item = items[indexPath.row]
        
        cell.itemTitle.text = item["subtype"]?.capitalized
        cell.itemImage.image = UIImage.init(imageLiteralResourceName: String(item["subtype"]! + "Icon"))
        cell.itemDetail.text = item["description"]?.capitalized
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        guard let header = view as? UITableViewHeaderFooterView else { return }
        header.textLabel?.textColor = UIColor.black
        header.textLabel?.font = UIFont.init(name: "Avenir", size: 14)
        //        header.textLabel?.frame = header.frame
        header.textLabel?.frame = CGRect(x: 20, y: 8, width: 320, height: 25)
        header.textLabel?.textAlignment = .left
    }
    
    // Deselect row after touch cell
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableview.deselectRow(at: indexPath, animated: true)
        
        let items = self.categorySections[indexPath.section].items
        let item = items[indexPath.row]["subtype"]!
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        if item == "sleep" {
            let destinationViewController = storyBoard.instantiateViewController(withIdentifier: "addSleepViewController") as! AddSleepViewController
            self.navigationController?.pushViewController(destinationViewController, animated: true)
        } else if item == "medicine" {
            let destinationViewController = storyBoard.instantiateViewController(withIdentifier: "addMedicineViewController") as! AddMedicineViewController
            self.navigationController?.pushViewController(destinationViewController, animated: true)
        } else {
            let destinationViewController = storyBoard.instantiateViewController(withIdentifier: "addActivityViewController") as! AddActivityViewController
            print(items[indexPath.row])
            destinationViewController.type = self.categorySections[indexPath.section].name
            destinationViewController.subtype = item
            destinationViewController.one = items[indexPath.row]["one"]!
            destinationViewController.two = items[indexPath.row]["two"]!
            destinationViewController.three = items[indexPath.row]["three"]!
            destinationViewController.four = items[indexPath.row]["four"]!
            destinationViewController.five = items[indexPath.row]["five"]!
            self.navigationController?.pushViewController(destinationViewController, animated: true)
        }
    }
}
