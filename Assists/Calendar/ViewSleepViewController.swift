//
//  ViewSleepViewController.swift
//  Assists
//
//  Created by Thunpisit Amnuaikiatloet on 9/4/19.
//  Copyright © 2019 Thunpisit Amnuaikiatloet. All rights reserved.
//

import UIKit
import SwiftDate
import Eureka

class ViewSleepViewController: FormViewController {

    var data: Sleep!
    @IBOutlet weak var dataTime: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = getDateTime(isoDate: data.date)
        setupForm()
        self.navigationController!.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: "Avenir-Medium", size: 17)!]
    }
    
    func setupForm(){
        
        LabelRow.defaultCellUpdate = { cell, row in
            cell.contentView.backgroundColor = .white
            cell.textLabel?.textColor = .black
            cell.textLabel?.font = UIFont.init(name: "Avenir", size: 14)
            cell.textLabel?.textAlignment = .right
            
        }
        
        TextRow.defaultCellUpdate = { cell, row in
            cell.titleLabel?.textColor = .black
        }
        
        form
            
            +++ Section("Sleep information")
            
            <<< LabelRow () {
                $0.title = "Time"
                $0.value = getTime(isoDate: data.date)
                }.cellUpdate { cell, row in
                    cell.textLabel?.font = UIFont.init(name: "Avenir", size: 14)
                    cell.detailTextLabel?.font = UIFont.init(name: "Avenir", size: 14)
                    cell.detailTextLabel?.font = UIFont.init(name: "Avenir", size: 14)
                    //                    cell.tintColor = UIColor(red:0.24, green:0.37, blue:0.91, alpha:1.0)
            }
            
            <<< LabelRow () {
                $0.title = "Duration"
                $0.value = data.duration + " hours"
                }.cellUpdate { cell, row in
                    cell.textLabel?.font = UIFont.init(name: "Avenir", size: 14)
                    cell.detailTextLabel?.font = UIFont.init(name: "Avenir", size: 14)
                    cell.detailTextLabel?.font = UIFont.init(name: "Avenir", size: 14)
                    //                    cell.tintColor = UIColor(red:0.24, green:0.37, blue:0.91, alpha:1.0)
            }
            
            <<< LabelRow () {
                $0.title = "Depth"
                $0.value = data.depth.capitalized
                }.cellUpdate { cell, row in
                    cell.textLabel?.font = UIFont.init(name: "Avenir", size: 14)
                    cell.detailTextLabel?.font = UIFont.init(name: "Avenir", size: 14)
                    cell.detailTextLabel?.font = UIFont.init(name: "Avenir", size: 14)
                    //                    cell.tintColor = UIColor(red:0.24, green:0.37, blue:0.91, alpha:1.0)
            }
            
            <<< LabelRow () {
                $0.title = "Interuption"
                $0.value = data.interuption.capitalized
                }.cellUpdate { cell, row in
                    cell.textLabel?.font = UIFont.init(name: "Avenir", size: 14)
                    cell.detailTextLabel?.font = UIFont.init(name: "Avenir", size: 14)
                    cell.detailTextLabel?.font = UIFont.init(name: "Avenir", size: 14)
                    //                    cell.tintColor = UIColor(red:0.24, green:0.37, blue:0.91, alpha:1.0)
            }
            
            <<< LabelRow () {
                $0.title = "Snoring"
                $0.value = data.snoring.capitalized
                }.cellUpdate { cell, row in
                    cell.textLabel?.font = UIFont.init(name: "Avenir", size: 14)
                    cell.detailTextLabel?.font = UIFont.init(name: "Avenir", size: 14)
                    cell.detailTextLabel?.font = UIFont.init(name: "Avenir", size: 14)
                    //                    cell.tintColor = UIColor(red:0.24, green:0.37, blue:0.91, alpha:1.0)
            }
            
            <<< LabelRow () {
                $0.title = "Time to sleep"
                $0.value = data.timeToSleep + "mins"
                }.cellUpdate { cell, row in
                    cell.textLabel?.font = UIFont.init(name: "Avenir", size: 14)
                    cell.detailTextLabel?.font = UIFont.init(name: "Avenir", size: 14)
                    cell.detailTextLabel?.font = UIFont.init(name: "Avenir", size: 14)
                    //                    cell.tintColor = UIColor(red:0.24, green:0.37, blue:0.91, alpha:1.0)
            }
            
            <<< LabelRow () {
                $0.title = "Time to get up"
                $0.value = data.timeToWake + "mins"
                }.cellUpdate { cell, row in
                    cell.textLabel?.font = UIFont.init(name: "Avenir", size: 14)
                    cell.detailTextLabel?.font = UIFont.init(name: "Avenir", size: 14)
                    cell.detailTextLabel?.font = UIFont.init(name: "Avenir", size: 14)
                    //                    cell.tintColor = UIColor(red:0.24, green:0.37, blue:0.91, alpha:1.0)
            }
            
            <<< TextAreaRow() {
                $0.placeholder = "Notes"
                $0.textAreaHeight = .dynamic(initialTextViewHeight: 100)
                $0.value = data.notes
                }.cellUpdate { cell, row in
                    cell.textLabel?.font = UIFont.init(name: "Avenir", size: 14)
                    cell.detailTextLabel?.font = UIFont.init(name: "Avenir", size: 14)
                    cell.detailTextLabel?.font = UIFont.init(name: "Avenir", size: 14)
                    //                    cell.tintColor = UIColor(red:0.24, green:0.37, blue:0.91, alpha:1.0)
                    cell.placeholderLabel?.font = UIFont.init(name: "Avenir", size: 14)
                    cell.textView?.font = UIFont.init(name: "Avenir", size: 14)
                    row.baseCell.isUserInteractionEnabled = false
        }
        
    }
    
    func getTime(isoDate: String) -> String {
        
        let formatDate = isoDate.toDate()
        let localDate = formatDate!.convertTo(region: Region.local)
        let date = "\(localDate.hour)" + ":" + "\(localDate.minute)"
        
        return date
    }
    
    func getDateTime(isoDate: String) -> String {
        
        let formatDate = isoDate.toDate()
        let localDate = formatDate!.convertTo(region: Region.local)
        let date = localDate.toFormat("MMMM dd, HH:mm")
        
        return date
    }
    
}

