//
//  NotificationViewController.swift
//  Assists
//
//  Created by Thunpisit Amnuaikiatloet on 9/3/19.
//  Copyright © 2019 Thunpisit Amnuaikiatloet. All rights reserved.
//

import UIKit
//import MSPeekCollectionViewDelegateImplementation

class NotificationViewController: UIViewController {

//    @IBOutlet weak var newsCollectionView: UICollectionView!
    
//    var delegate: MSPeekCollectionViewDelegateImplementation!
    
    var notiCounts = 3
    
    override func viewDidLoad() {
        super.viewDidLoad()

        style()
        
        if notiCounts == 0 {
            title = "No notification"
        } else if notiCounts == 1 {
            title = "1 notification"
        } else {
            title = String(notiCounts) + " notifications"
        }
        
//        newsCollectionView.configureForPeekingDelegate()
//        newsCollectionView.delegate = delegate
//        newsCollectionView.dataSource = self
        
    }
    
    func style() {
        // + CUSTOM NAVIGATION CONTROLLER
        // Change navigation controller button to grey
        self.navigationController?.navigationBar.tintColor = UIColor(red:0.02, green:0.17, blue:0.36, alpha:1.0)
        // Change navigation controller bar to white
        self.navigationController?.navigationBar.barTintColor = UIColor.white
        // Change navigationContrller font to Avenir
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: "Avenir-Medium", size: 16)!]
        // Hide navigationController hairline
        self.navigationController?.navigationBar.setValue(true, forKey: "hidesShadow")
        self.navigationController?.navigationBar.isTranslucent = false
        
        // + CUSTOM TABBAR CONTROLLER
        // Change tabBar color to grey
        self.tabBarController?.tabBar.tintColor = UIColor(red:0.24, green:0.37, blue:0.91, alpha:1.0)
        // Change tabBar font to Avenir
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font: UIFont(name: "Avenir", size: 10)!], for: .normal)
        
        // + CUSTOM VIEW CONTROLLER
        // Change background color
        self.view.backgroundColor = UIColor(red:0.93, green:0.93, blue:0.93, alpha:1.0)
    }

}

//extension NotificationViewController: UICollectionViewDataSource {
//    func numberOfSections(in collectionView: UICollectionView) -> Int {
//        return 1
//    }
//
//    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        return 4
//    }
//
//    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath)
//        cell.contentView.backgroundColor = UIColor.red
//        return cell
//    }
//}
//
//extension NotificationViewController: MSPeekImplementationDelegate {
//    func peekImplementation(_ peekImplementation: MSPeekCollectionViewDelegateImplementation, didChangeActiveIndexTo activeIndex: Int) {
//        print("Changed active index to \(activeIndex)")
//    }
//
//    func peekImplementation(_ peekImplementation: MSPeekCollectionViewDelegateImplementation, didSelectItemAt indexPath: IndexPath) {
//        print("Selected item at \(indexPath)")
//    }
//}
