//
//  CalendarViewController.swift
//  Assists
//
//  Created by Thunpisit Amnuaikiatloet on 8/29/19.
//  Copyright © 2019 Thunpisit Amnuaikiatloet. All rights reserved.
//

import UIKit
import SwiftDate
import JTAppleCalendar

class CalendarViewController: UIViewController {
    
    @IBOutlet var calendarView: JTAppleCalendarView!
    @IBOutlet weak var constraint: NSLayoutConstraint!
    @IBOutlet weak var tableview: UITableView!
    
    var events: [Event]? = []
    var event = Event(date: "2019-09-09")
    var emptyEvent = Event(date: "2019-09-09")
    let categorySections = ["Last night sleep","Today activity","Medicine taken"]
    var numberOfRows = 1
    
    var patientId = UserDefaults.standard.string(forKey: "patient_id") ?? "null"
    var userKey = UserDefaults.standard.string(forKey: "user_key") ?? "null"
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)

        // + Setup today calendar
        configureToday()
        configureCalendar()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        style()
        
        // Setup initial calendar
        configureToday()
        configureCalendar()
        
        self.tableview.addSubview(self.refreshControl)
        tableview.rowHeight = UITableView.automaticDimension
        startLoading()
        
        DispatchQueue.main.async {
            
            let systemDate = Date();
            let localDate = systemDate.convertTo(region: Region.local)
            let todayDate = localDate.toFormat("YYYY-MM-dd")
            
            self.updateData()
            self.updateDate(targetDate: todayDate)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                self.tableview.reloadData()
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    func updateData() {
        DispatchQueue.main.async {
            // Request events and arrarnge data into struct
            requestEvents(pid: self.patientId, key: self.userKey, completionHandler: { (result) in
                self.events = result
            })
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                self.tableview.reloadData()
            }
        }
    }
    
    func updateDate(targetDate: String) {
        
        self.event = self.emptyEvent
        self.tableview.reloadData()
        
        DispatchQueue.main.async {
            requestEvents(pid: self.patientId, key: self.userKey, completionHandler: { (result) in
                self.events = result
                for e in self.events! {
                    if e.date == targetDate {
                        self.event = e
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
//                            print(self.event)
                            self.tableview.reloadData()
                        }
                    }
                }
            })
        }
        
    }
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(CalendarViewController.handleRefresh(refreshControl:)), for: UIControl.Event.valueChanged)
        
        return refreshControl
    }()
    
    @objc func handleRefresh(refreshControl: UIRefreshControl) {
        
        self.updateData()
        refreshControl.endRefreshing()
    }
    
    func startLoading() {
        let alert = UIAlertController(title: nil, message: "Loading...", preferredStyle: .alert)
        
        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = UIActivityIndicatorView.Style.gray
        loadingIndicator.startAnimating();
        
        alert.view.addSubview(loadingIndicator)
        present(alert, animated: true, completion: nil)
    }
    
    func configureToday() {
        self.numberOfRows = 1
        self.calendarView.scrollToDate(Date(),animateScroll: false)
        self.calendarView.selectDates([ Date() ])
        self.calendarView.reloadData(withanchor: Date())
    }
    
    func configureCalendar() {
        calendarView.scrollDirection = .horizontal
        calendarView.scrollingMode   = .stopAtEachCalendarFrame
        calendarView.showsHorizontalScrollIndicator = false
    }
    
    func configureCell(view: JTAppleCell?, cellState: CellState) {
        guard let cell = view as? DateCell  else { return }
        cell.dateLabel.text = cellState.text
        handleCellTextColor(cell: cell, cellState: cellState)
        handleCellSelected(cell: cell, cellState: cellState)
//        handleCellTodayDate(cell: cell, cellState: cellState)
    }
    
    func handleCellTextColor(cell: DateCell, cellState: CellState) {
        if cellState.dateBelongsTo == .thisMonth {
            cell.dateLabel.textColor = UIColor.black
        } else {
            cell.dateLabel.textColor = UIColor.gray
        }
    }
    
    func handleCellSelected(cell: DateCell, cellState: CellState) {
        if cellState.isSelected {
            cell.selectedView.layer.cornerRadius =  15
            cell.selectedView.isHidden = false
            cell.dateLabel.textColor = UIColor.white
            cell.selectedView.layer.backgroundColor = UIColor(red:0.24, green:0.37, blue:0.91, alpha:1.0).cgColor
        } else {
            cell.selectedView.isHidden = true
        }
    }
    
    func style() {
        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        } else {
            // Fallback on earlier versions
        }  
        // + CUSTOM NAVIGATION CONTROLLER
        // Change navigation controller button to grey
        self.navigationController?.navigationBar.tintColor = UIColor(red:0.02, green:0.17, blue:0.36, alpha:1.0)
        // Change navigation controller bar to white
        self.navigationController?.navigationBar.barTintColor = UIColor.white
        // Change navigationContrller font to Avenir
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: "Avenir-Medium", size: 16)!]
        // Hide navigationController hairline
        self.navigationController?.navigationBar.setValue(true, forKey: "hidesShadow")
        self.navigationController?.navigationBar.isTranslucent = false
        
        // + CUSTOM VIEW CONTROLLER
        // Change background color
        self.view.backgroundColor = UIColor.white
    }
    
    @IBAction func todayToggle(_ sender: Any) {
        configureToday()
        configureCalendar()
        self.calendarView.scrollToDate(Date(),animateScroll: true)
        self.calendarView.selectDates([ Date() ])
        self.calendarView.reloadData(withanchor: Date())
    }
    
}

extension CalendarViewController: JTAppleCalendarViewDataSource {
    
    func configureCalendar(_ calendar: JTAppleCalendarView) -> ConfigurationParameters {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy MM dd"
//        var dateComponent = DateComponents()
//        dateComponent.year = 2
        let startDate = formatter.date(from: "2018 01 01")!
//        let endDate = Date()
        let endDate = formatter.date(from: "2099 01 01")!
        if numberOfRows == 6 {
            return ConfigurationParameters(startDate: startDate, endDate: endDate, numberOfRows: numberOfRows)
        } else {
            return ConfigurationParameters(startDate: startDate,
                                           endDate: endDate,
                                           numberOfRows: numberOfRows,
                                           generateInDates: .forFirstMonthOnly,
                                           generateOutDates: .off,
                                           hasStrictBoundaries: false)
        }
    }
}

extension CalendarViewController: JTAppleCalendarViewDelegate {
    
    func calendar(_ calendar: JTAppleCalendarView, cellForItemAt date: Date, cellState: CellState, indexPath: IndexPath) -> JTAppleCell {
        let cell = calendar.dequeueReusableJTAppleCell(withReuseIdentifier: "dateCell", for: indexPath) as! DateCell
        self.calendar(calendar, willDisplay: cell, forItemAt: date, cellState: cellState, indexPath: indexPath)
        self.navigationItem.title = date.monthName(.default)
        
        return cell
    }
    
    func calendar(_ calendar: JTAppleCalendarView, willDisplay cell: JTAppleCell, forItemAt date: Date, cellState: CellState, indexPath: IndexPath) {
        
        configureCell(view: cell, cellState: cellState)
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didSelectDate date: Date, cell: JTAppleCell?, cellState: CellState) {
        configureCell(view: cell, cellState: cellState)
        
        // Handle request data from selected date and show in TableView
        let selectedDate = date.toFormat("YYYY-MM-dd")
        self.updateDate(targetDate: selectedDate)
        
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didDeselectDate date: Date, cell: JTAppleCell?, cellState: CellState) {
        configureCell(view: cell, cellState: cellState)
    }
    
}

extension CalendarViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.categorySections.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return self.categorySections[section].capitalized
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        var numSection: Int = 0
        
        if section == 0 {
            
            if event.sleeps.count == 0 {
                numSection = 1
            } else {
                numSection = event.sleeps.count
            }
            
        } else if section == 1 {
            
            if event.activities.count == 0 {
                numSection = 1
            } else {
                numSection = event.activities.count
            }
            
        } else if section == 2 {
            
            if event.medicines.count == 0 {
                numSection = 1
            } else {
                numSection = event.medicines.count
            }
        }
        
        return numSection
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "itemCell") as! ItemTableViewCell
        
        if indexPath.section == 0 {
            
            if event.sleeps.count == 0 {
                cell.itemName.text = "NO SLEEP RECORD"
                cell.itemDetail.text = "There are no record."
                cell.itemDate.text = ""
                cell.itemImage.image = UIImage.init(imageLiteralResourceName: String("emptyIcon"))
            } else {
                let item = event.sleeps[indexPath.row]
                cell.itemName.text = item.depth.capitalized
                cell.itemDetail.text = item.duration.capitalized + " Hours"
                cell.itemDate.text = getTime(isoDate: item.date)
                cell.itemImage.image = UIImage.init(imageLiteralResourceName: String("sleepIcon"))
                cell.accessoryType = .disclosureIndicator
            }
            
        } else if indexPath.section == 1 {
            
            
            if event.activities.count == 0 {
                cell.itemName.text = "NO ACTIVITY RECORD"
                cell.itemDetail.text = "There are no record."
                cell.itemDate.text = ""
                cell.itemImage.image = UIImage.init(imageLiteralResourceName: String("emptyIcon"))
            } else {
                let item = event.activities[indexPath.row]
                cell.itemName.text = item.type.capitalized
                cell.itemDetail.text = item.subtype.capitalized
                cell.itemDate.text = getTime(isoDate: item.date)
                cell.itemImage.image = UIImage.init(imageLiteralResourceName: String(item.subtype + "Icon"))
                cell.accessoryType = .disclosureIndicator
            }
            
        } else if indexPath.section == 2 {
            
            if event.medicines.count == 0 {
                cell.itemName.text = "NO MEDICINE RECORD"
                cell.itemDetail.text = "There are no record."
                cell.itemDate.text = ""
                cell.itemImage.image = UIImage.init(imageLiteralResourceName: String("emptyIcon"))
            } else {
                let item = event.medicines[indexPath.row]
                cell.itemName.text = item.name.capitalized
                cell.itemDetail.text = item.dosage
                cell.itemDate.text = getTime(isoDate: item.date)
                cell.itemImage.image = UIImage.init(imageLiteralResourceName: String("medicineIcon"))
                cell.accessoryType = .disclosureIndicator
            }
            
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        guard let header = view as? UITableViewHeaderFooterView else { return }
        header.textLabel?.textColor = UIColor.black
        header.tintColor = UIColor.white
        header.textLabel?.font = UIFont.init(name: "Avenir", size: 16)
        //        header.textLabel?.frame = header.frame
        header.textLabel?.frame = CGRect(x: 20, y: 8, width: 320, height: 25)
        header.textLabel?.textAlignment = .left
    }
    
    // Deselect row after touch cell
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableview.deselectRow(at: indexPath, animated: true)
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        
        if indexPath.section == 0 {
            
            if event.sleeps.count != 0 {
                let item = event.sleeps[indexPath.row]
                let nextViewController = storyBoard.instantiateViewController(withIdentifier: "viewSleepViewController") as! ViewSleepViewController
                nextViewController.data = item
                navigationController?.pushViewController(nextViewController, animated: true)
            }
            
        } else if indexPath.section == 1 {
            if event.activities.count != 0 {
                let item = event.activities[indexPath.row]
                let nextViewController = storyBoard.instantiateViewController(withIdentifier: "viewActivityViewController") as! ViewActivityViewController
                nextViewController.data = item
                navigationController?.pushViewController(nextViewController, animated: true)
            }
        } else if indexPath.section == 2 {
            if event.medicines.count != 0 {
                let item = event.medicines[indexPath.row]
                let nextViewController = storyBoard.instantiateViewController(withIdentifier: "viewMedicineViewController") as! ViewMedicineViewController
                nextViewController.data = item
                navigationController?.pushViewController(nextViewController, animated: true)
            }
        }
    }
    
    func getTime(isoDate: String) -> String {
        
        let formatDate = isoDate.toDate()
        let localDate = formatDate!.convertTo(region: Region.local)
        let date = "\(localDate.hour)" + ":" + "\(localDate.minute)"
        
        return date
    }
}


