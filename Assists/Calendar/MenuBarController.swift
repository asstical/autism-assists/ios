//
//  MenuBarController.swift
//  Assists
//
//  Created by Thunpisit Amnuaikiatloet on 8/29/19.
//  Copyright © 2019 Thunpisit Amnuaikiatloet. All rights reserved.
//

import UIKit

class MenuBarController: UITabBarController, UITabBarControllerDelegate {
    
//    var calendarViewController: CalendarViewController!
//    var dashboardViewController: DashboardViewController!
//    var addViewController: AddViewController!
//    var chatViewController: ChatViewController!
//    var profileViewController: ProfileViewController!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Change tabBar font to Avenir
        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        } else {
            // Fallback on earlier versions
        }
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font: UIFont(name: "Avenir-Medium", size: 10)!], for: .normal)
        // Change tabBar color to blue
        UITabBar.appearance().tintColor = UIColor(red:0.24, green:0.37, blue:0.91, alpha:1.0)
        
        // Change translucent to false
        tabBar.isTranslucent = false
        
        // Remove hairline
        tabBar.shadowImage = UIImage()
        tabBar.backgroundImage = UIImage()
    }
    
    // Set action for center tab
//    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
//        if viewController.isKind(of: AddViewController.self) {
//            let vc =  AddViewController()
//            vc.modalPresentationStyle = .overFullScreen
//            self.present(vc, animated: true, completion: nil)
//            return false
//        }
//        return true
//    }

}
