//
//  ItemTableViewCell.swift
//  Assists
//
//  Created by Thunpisit Amnuaikiatloet on 9/3/19.
//  Copyright © 2019 Thunpisit Amnuaikiatloet. All rights reserved.
//

import UIKit

class ItemTableViewCell: UITableViewCell {

    @IBOutlet weak var itemImage: UIImageView!
    @IBOutlet weak var itemName: UILabel!
    @IBOutlet weak var itemDetail: UILabel!
    @IBOutlet weak var itemDate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func prepareForReuse() {
        itemName.text = nil
        itemDetail.text = nil
        itemDate.text = nil
    }

}
