# Capturing and Managing Daily Symptoms Data in the Treatment of Autism Spectrum Disorder Using Mobile Technology: A Pilot Study - iOS Application

An iOS application for the care team to enter the input of patients on daily basis. Written in Native iOS Swift 4.X on Xcode. The developed and test product with University of Missouri under Thunpisit Amnuaikiatloet, Dr.Fang Wang, and Dr.Kristin Sohl

# Requirements
* Xcode with Swift 4.X+
* CocoaPods

# Developer
* **Thunpisit Amnuaikiatloet** (thunpisit.am@gmail.com , thunpisit@mail.missouri.edu)
* **Fang Wang** (wangfan@missouri.edu)
